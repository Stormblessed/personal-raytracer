#include "Ambient.h"

Ambient::Ambient(void) : Light(), color(1.0f)
{
	intensity = 1.0f;
}

Ambient::Ambient(const Ambient& a) : Light(a), color(a.color)
{
	intensity = a.intensity;
}

Ambient::~Ambient(void)
{
}

Ambient& Ambient::operator= (const Ambient& rhs) 
{
	if(this == &rhs) return *this;

	Light::operator=(rhs);
	
	intensity = rhs.intensity; 
	color = rhs.color;

	return *this;
}

Vector3d Ambient::GetDirection(ShadeRec& sr)
{
	return Vector3d(0.0f);
}

RGBColor Ambient::Radiance(ShadeRec& sr)
{
	return intensity * color;
}

bool Ambient::InShadow(const Ray& ray, const ShadeRec& sr) const
{
	return false;
}

