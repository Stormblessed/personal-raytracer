#pragma once
#include "Light.h"

class Ambient : public Light
{
public:
	Ambient(void);
	Ambient(const Ambient&);
	~Ambient(void);

	Ambient& operator=(const Ambient&);

	virtual Vector3d GetDirection(ShadeRec&);
	virtual RGBColor Radiance(ShadeRec&);

	void SetIntensity(float i) {intensity = i;}
	void SetColor(RGBColor c) {color = c;}
	void SetColor(float r, float g, float b) {color = RGBColor(r, g, b);}
	void SetColor(float c) {color = RGBColor(c);}

	virtual bool InShadow(const Ray&, const ShadeRec&) const;

private:
	float intensity;
	RGBColor color;
};

