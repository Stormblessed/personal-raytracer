#include "AmbientOccluder.h"
#include "World.h"

AmbientOccluder::AmbientOccluder(void)
{
}


AmbientOccluder::~AmbientOccluder(void)
{
}

void AmbientOccluder::SetSampler(Sampler* s_ptr)
{
	if(sampler_ptr)
	{
		delete sampler_ptr;
		sampler_ptr = NULL;
	}
	sampler_ptr = s_ptr;
	sampler_ptr->MapSamplesToHemisphere(1);
}

Vector3d AmbientOccluder::GetDirection(ShadeRec& sr)
{
	Point3d sp = sampler_ptr->SampleHemisphere();
	return (sp.x * u + sp.y * v + sp.z * w);
}

bool AmbientOccluder::InShadow(const Ray& ray, const ShadeRec& sr) const
{
	float t;
	for(int i = 0; i < sr.world.objects.size(); i++)
	{
		if(sr.world.objects[i]->ShadowHit(ray, t)) return true;
	}
	return false;
}

RGBColor AmbientOccluder::Radiance(ShadeRec& sr)
{
	w = sr.normal;
	//Jitter up vector to handle a vertical normal
	v = w ^ Vector3d(0.0072, 1.0, 0.0034);
	v.Normalize();
	u = v ^ w;

	Ray shadowRay;
	shadowRay.origin = sr.hitPoint;
	shadowRay.direction = GetDirection(sr);

	if(InShadow(shadowRay, sr)) return (minAmount * intensity * color);
	else return (intensity * color);
}