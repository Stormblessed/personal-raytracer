#pragma once
#include "Light.h"
#include "Sampler.h"

class AmbientOccluder : public Light
{
public:
	AmbientOccluder(void);
	~AmbientOccluder(void);

	void SetIntensity(float i) {intensity = i;}
	void SetColor(RGBColor c) {color = c;}
	void SetColor(float r, float g, float b) {color = RGBColor(r, g, b);}
	void SetColor(float c) {color = RGBColor(c);}
	void SetMinAmount(float min) {minAmount = min;}

	void SetSampler(Sampler* s_ptr);
	virtual Vector3d GetDirection(ShadeRec& sr);
	virtual bool InShadow(const Ray& ray, const ShadeRec& sr) const;
	virtual RGBColor Radiance(ShadeRec& sr);

private:
	float intensity;
	RGBColor color;
	Vector3d u, v, w;
	Sampler* sampler_ptr;
	RGBColor minAmount;
};

