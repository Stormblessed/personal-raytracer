#include "AreaLight.h"
#include "World.h"
#include "GeometricObject.h"

AreaLight::AreaLight(void) : Light(), object_ptr(NULL), material_ptr(NULL)
{
}

AreaLight::~AreaLight(void)
{
	if(object_ptr) 
	{
		delete object_ptr;
		object_ptr = NULL;
	}
	if(material_ptr) 
	{
		delete material_ptr;
		material_ptr = NULL;
	}
}

Vector3d AreaLight::GetDirection(ShadeRec& sr)
{
	samplePoint = object_ptr->Sample();
	lightNormal = object_ptr->GetNormal(samplePoint);
	wi = samplePoint - sr.hitPoint;
	wi.Normalize();

	return wi;
}

bool AreaLight::InShadow(const Ray& ray, const ShadeRec& sr) const
{
	float t;
	float ts = (samplePoint - ray.origin) * ray.direction;

	for(int i = 0; i < sr.world.objects.size(); i++)
	{
		if(sr.world.objects[i]->ShadowHit(ray, t) && t < ts) return true;
	}
	return false;
}

RGBColor AreaLight::Radiance(ShadeRec& sr)
{
	float ndotd = -lightNormal * wi;

	if(ndotd > 0.0f)
	{
		return material_ptr->GetEmissiveRadiance(sr);
	}
	else
	{
		return black;
	}
}

float AreaLight::GeometricFactor(const ShadeRec& sr)
{
	float ndotd = -lightNormal * wi;
	float d2 = samplePoint.DistanceSquared(sr.hitPoint);

	return ndotd / d2;
}

float AreaLight::PDF(const ShadeRec& sr) const
{
	ShadeRec srNotConst = sr;
	return object_ptr->PDF(srNotConst);
}