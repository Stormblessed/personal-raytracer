#pragma once
#include "Light.h"
#include "RGBColor.h"#include "Vector3d.h"#include "Point3d.h"#include "Normal.h"#include "Material.h"
class GeometricObject;

class AreaLight : public Light
{
public:
	AreaLight(void);
	~AreaLight(void);

	virtual Vector3d GetDirection(ShadeRec& sr);
	virtual bool InShadow(const Ray& ray, const ShadeRec& sr) const;

	virtual RGBColor Radiance(ShadeRec& sr);
	virtual float GeometricFactor(const ShadeRec& sr);
	virtual float PDF(const ShadeRec& sr) const;

	void SetObject(GeometricObject* obj) {object_ptr = obj;}
	void SetMaterial(Material* mat) {material_ptr = mat;}
	void SetSamplePoint(Point3d p) {samplePoint = p;}
	void SetLightNormal(Normal n) {lightNormal = n;}

private:
	GeometricObject* object_ptr;
	Material* material_ptr;
	Point3d samplePoint;
	Normal lightNormal;
	Vector3d wi;
};

