#include "AreaLighting.h"
#include "World.h"
#include "ShadeRec.h"
#include "Material.h"

AreaLighting::AreaLighting(void) : Tracer()
{
}

AreaLighting::AreaLighting(World* world_ptr) : Tracer(world_ptr)
{
}

AreaLighting::~AreaLighting(void)
{
}

RGBColor AreaLighting::TraceRay(const Ray ray, const int depth) const
{
	if(depth > world_ptr->vp.maxDepth)
	{
		return black;
	}
	else
	{
		ShadeRec sr(world_ptr->HitObjects(ray));
		
		if(sr.hitAnObject)
		{
			sr.depth = depth;
			sr.ray = ray;

			return sr.material_ptr->AreaLightShade(sr);
		}
		else
		{
			return world_ptr->backgroundColor;
		}
	}
}
