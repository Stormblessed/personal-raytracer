#pragma once
#include "Tracer.h"

class AreaLighting : public Tracer
{
public:
	AreaLighting(void);
	AreaLighting(World* world_ptr);
	virtual ~AreaLighting(void);

	virtual RGBColor TraceRay(const Ray ray, const int depth) const;
};

