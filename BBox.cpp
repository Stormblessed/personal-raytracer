#include "BBox.h"
#include "Constants.h"

BBox::BBox(void) : x0(-1), x1(1), y0(-1), y1(1), z0(-1), z1(1)
{
}	

BBox::BBox(const double _x0, const double _x1, const double _y0, const double _y1, const double _z0, const double _z1)
: x0(_x0), x1(_x1), y0(_y0), y1(_y1), z0(_z0), z1(_z1)
{
}

BBox::BBox(const Point3d p0, const Point3d p1) : x0(p0.x), x1(p1.x), y0(p0.y), y1(p1.y), z0(p0.z), z1(p1.z)
{
}
										
BBox::BBox(const BBox& bbox) : x0(bbox.x0), x1(bbox.x1), y0(bbox.y0), y1(bbox.y1), z0(bbox.z0), z1(bbox.z1)
{
}						

BBox::~BBox(void)
{
}

BBox& BBox::operator=(const BBox& rhs) 
{
	if(this == &rhs) return *this;

	x0	= rhs.x0;
	x1	= rhs.x1;
	y0	= rhs.y0;
	y1	= rhs.y1;
	z0	= rhs.z0;
	z1	= rhs.z1;	
	
	return *this;
}			

bool BBox::Hit(const Ray& ray) const
{	
	double ox = ray.origin.x; 
	double oy = ray.origin.y; 
	double oz = ray.origin.z;
	double dx = ray.direction.x; 
	double dy = ray.direction.y; 
	double dz = ray.direction.z;
	
	double txMin, tyMin, tzMin;
	double txMax, tyMax, tzMax; 

	double a = 1.0 / dx;
	if(a >= 0) 
	{
		txMin = (x0 - ox) * a;
		txMax = (x1 - ox) * a;
	}
	else 
	{
		txMin = (x1 - ox) * a;
		txMax = (x0 - ox) * a;
	}
	
	double b = 1.0 / dy;
	if(b >= 0) 
	{
		tyMin = (y0 - oy) * b;
		tyMax = (y1 - oy) * b;
	}
	else 
	{
		tyMin = (y1 - oy) * b;
		tyMax = (y0 - oy) * b;
	}
	
	double c = 1.0 / dz;
	if(c >= 0) 
	{
		tzMin = (z0 - oz) * c;
		tzMax = (z1 - oz) * c;
	}
	else 
	{
		tzMin = (z1 - oz) * c;
		tzMax = (z0 - oz) * c;
	}
	
	double t0, t1;
		
	if(txMin > tyMin)
	{
		t0 = txMin;
	}
	else
	{
		t0 = tyMin;
	}
	
	if(tzMin > t0)
	{
		t0 = tzMin;
	}
				
	if(txMax < tyMax)
	{
		t1 = txMax;
	}
	else
	{
		t1 = tyMax;
	}
		
	if(tzMax < t1)
	{
		t1 = tzMax;
	}
		
	return (t0 < t1 && t1 > kEpsilon);
}

bool BBox::Contains(const Point3d& p) const 
{
	return (p.x > x0 && p.x < x1) && (p.y > y0 && p.y < y1) && (p.z > z0 && p.z < z1);
};




