#pragma once
#include "Ray.h"
#include "Point3d.h"

class BBox
{
public:
	BBox(void);
	BBox(const double _x0, const double _x1, const double _y0, const double _y1, const double _z0, const double _z1);
	BBox(const Point3d p0, const Point3d p1);
	BBox(const BBox& BBox);
	~BBox(void);

	BBox& operator=(const BBox& rhs);

	bool Hit(const Ray& ray) const;
	bool Contains(const Point3d& p) const;

public:
	double x0, x1, y0, y1, z0, z1;
};

