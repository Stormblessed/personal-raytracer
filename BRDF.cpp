#include "BRDF.h"
#include "Constants.h"

BRDF::BRDF(void)
{
}

BRDF::BRDF(const BRDF& brdf) 
{
}	

BRDF::~BRDF(void) 
{
}  

RGBColor BRDF::F(const ShadeRec& sr, const Vector3d& wo, const Vector3d& wi) const 
{
	return black;
}

RGBColor BRDF::SampleF(const ShadeRec& sr, const Vector3d& wo, Vector3d& wi) const 
{
	return black;
}

RGBColor BRDF::SampleF(const ShadeRec& sr, const Vector3d& wo, Vector3d& wi, float& pdf) const 
{
	return black;
}
	
RGBColor BRDF::Rho(const ShadeRec& sr, const Vector3d& wo) const 
{
	return black;
}

BRDF& BRDF::operator= (const BRDF& rhs) 
{
	if(this == &rhs) return *this;
	return *this;
}


