#pragma once
#include "Sampler.h"
#include "RGBColor.h"
#include "ShadeRec.h"
#include "Vector3d.h"

class BRDF
{
public:
	BRDF(void);
	BRDF(const BRDF& object);
	~BRDF(void);

	virtual RGBColor F(const ShadeRec&, const Vector3d&, const Vector3d&) const;
	virtual RGBColor SampleF(const ShadeRec&, const Vector3d&, Vector3d&) const;
	virtual RGBColor SampleF(const ShadeRec&, const Vector3d&, Vector3d&, float&) const;
	virtual RGBColor Rho(const ShadeRec&, const Vector3d&) const;

protected:
	BRDF& operator=(const BRDF&);
};