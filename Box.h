#pragma once
#include "GeometricObject.h"

class Box : public GeometricObject
{
public:
	Box(void);
	Box(const double _x0, const double _x1, const double _y0, const double _y1, const double _z0, const double _z1);
	Box(const Point3d p0, const Point3d p1);
	Box(const Box& box);
	~Box(void);

	virtual Box* Clone(void) const;

	virtual bool Hit(const Ray& ray, double& tMin, ShadeRec& sr) const;
	virtual bool ShadowHit(const Ray& ray, float& tMin) const;

	virtual Normal GetNormal(int face) const;

public:
	double x0, x1, y0, y1, z0, z1;
};

