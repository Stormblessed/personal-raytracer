#include "Camera.h"
#include <iostream>

Camera::Camera(void)
{
	eye = Point3d(0, 0, 500);
	lookAt = Point3d(0);
	rollAngle = 0.0f;
	up = Vector3d(0, 1, 0);
	u = Vector3d(1, 0, 0);
	v = Vector3d(0, 1, 0);
	w = Vector3d(0, 0, 1);
	exposureTime = 1.0f;
}

Camera::Camera(const Camera& c)
{
	eye	= c.eye;
	lookAt = c.lookAt;
	rollAngle = c.rollAngle;
	up = c.up;
	u = c.u;
	v = c.v;
	w = c.w;
	exposureTime = c.exposureTime;
}

Camera::~Camera(void)
{
}

Camera& Camera::operator= (const Camera& rhs) 
{
	if(this == &rhs) return *this;
	
	eye	= rhs.eye;
	lookAt = rhs.lookAt;
	rollAngle = rhs.rollAngle;
	up = rhs.up;
	u = rhs.u;
	v = rhs.v;
	w = rhs.w;
	exposureTime = rhs.exposureTime;

	return *this;
}

void Camera::ComputeUVW(void)
{
	w = eye - lookAt;
	w.Normalize();
	u = up ^ w;
	u.Normalize();
	v = w ^ u;

	if(eye.x == lookAt.x && eye.z == lookAt.z && eye.y > lookAt.y) // camera looking vertically down
	{ 
		u = Vector3d(0, 0, 1);
		v = Vector3d(1, 0, 0);
		w = Vector3d(0, 1, 0);	
	}
	
	if(eye.x == lookAt.x && eye.z == lookAt.z && eye.y < lookAt.y) // camera looking vertically up
	{ 
		u = Vector3d(1, 0, 0);
		v = Vector3d(0, 0, 1);
		w = Vector3d(0, -1, 0);
	}
}

void Camera::SetEye(const Point3d& p) 
{
	eye = p;
}

void Camera::SetEye(const float x, const float y, const float z) 
{
	eye.x = x; 
	eye.y = y;
	eye.z = z;
}

void Camera::SetLookAt(const Point3d& p) 
{
	lookAt = p;
}

void Camera::SetLookAt(const float x, const float y, const float z) 
{
	lookAt.x = x; 
	lookAt.y = y;
	lookAt.z = z;
}

void Camera::SetUpVector(const Vector3d& u) 
{
	up = u;
}

void Camera::SetUpVector(const float x, const float y, const float z) 
{
	up.x = x; 
	up.y = y; 
	up.z = z;
}

void Camera::SetRoll(const float r) 
{ 
	rollAngle = r;
}

void Camera::SetExposureTime(const float exposure) 
{
	exposureTime = exposure;
}
