#pragma once
#include "Point3d.h"
#include "Point2d.h"
#include "Vector3d.h"

class World;

class Camera
{
public:
	Camera(void);
	Camera(const Camera&);
	~Camera(void);

	Camera& operator=(const Camera&);

	void ComputeUVW(void);
	virtual void Render(World&) = 0;

	void SetEye(const Point3d&);
	void SetEye(const float, const float, const float);
	void SetLookAt(const Point3d&);
	void SetLookAt(const float, const float, const float);
	void SetUpVector(const Vector3d&);
	void SetUpVector(const float, const float, const float);
	void SetRoll(const float);
	void SetExposureTime(const float);

protected:
	Point3d eye;
	Point3d lookAt;
	Vector3d up;
	Vector3d u, v, w;
	float rollAngle;
	float exposureTime;
};
