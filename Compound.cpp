#include "Compound.h"
#include "Constants.h"

Compound::Compound(void) : GeometricObject()
{
}

Compound::Compound(const Compound& comp) : GeometricObject(comp) 
{
	CopyObjects(comp.objects);					
}

Compound::~Compound(void) 
{	
	ClearObjects();				
}

Compound* Compound::Clone(void) const 
{
	return new Compound(*this);
}

Compound& Compound::operator=(const Compound& rhs) 
{
	if(this == &rhs) return *this;

	GeometricObject::operator=(rhs);						
	CopyObjects(rhs.objects);				

	return *this;
}

void Compound::AddObject(GeometricObject* obj) 
{
	objects.push_back(obj);	
}

void Compound::SetMaterial(Material* mat) 
{
	for(int j = 0; j < objects.size(); j++)
	{
		objects[j]->SetMaterial(mat);
	}
}

int Compound::GetNumObjects(void) 
{
	return objects.size();
}

void Compound::ClearObjects(void) 
{	
	for(int j = 0; j < objects.size(); j++) 
	{
		delete objects[j];
		objects[j] = NULL;
	}	
	objects.erase(objects.begin(), objects.end());
}

void Compound::CopyObjects(const std::vector<GeometricObject*>& copyFrom) 
{
	ClearObjects();    		
	for(int j = 0; j < copyFrom.size(); j++)
	{
		objects.push_back(copyFrom[j]->Clone());
	}
}

bool Compound::Hit(const Ray& ray, double& tMin, ShadeRec& sr) const 
{
	double t; 
	Normal normal;
	Point3d localHitPoint;
	bool hit = false;
	tMin = kHugeValue;
	
	for(int j = 0; j < objects.size(); j++)
	{
		if(objects[j]->Hit(ray, t, sr) && (t < tMin)) 
		{
			hit = true;
			tMin = t;
			material_ptr = objects[j]->GetMaterial();
			normal = sr.normal;
			localHitPoint = sr.localHitPoint;  
		}
	}
	
	if(hit) 
	{
		sr.t = tMin;
		sr.normal = normal;   
		sr.localHitPoint = localHitPoint;  
	}
	
	return hit;
}

bool Compound::ShadowHit(const Ray& ray, float& tMin) const 
{
	float t;
	bool hit = false;
	tMin = kHugeValue;
	
	for(int j = 0; j < objects.size(); j++)
	{
		if(objects[j]->ShadowHit(ray, t) && (t < tMin)) 
		{
			hit = true;
			tMin = t;
			material_ptr = objects[j]->GetMaterial(); //May not need
		}
	}

	return hit;
}
