#pragma once
#include "GeometricObject.h"
#include <vector>

class Compound : public GeometricObject
{
public:
	Compound(void);
	Compound(const Compound& comp);
	~Compound(void);

	virtual Compound* Clone(void) const;
	Compound& operator=(const Compound& rhs);

	virtual void SetMaterial(Material* mat);
	virtual void AddObject(GeometricObject* obj);

	int GetNumObjects(void);
	
	virtual bool Hit(const Ray& ray, double& tMin, ShadeRec& sr) const;
	virtual bool ShadowHit(const Ray& ray, float& tMin) const;

protected:
	std::vector<GeometricObject*> objects;

private:
	void ClearObjects(void);
	void CopyObjects(const std::vector<GeometricObject*>& copyFrom);
};

