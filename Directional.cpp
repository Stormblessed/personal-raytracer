#include "Directional.h"

Directional::Directional(void) : Light(), color(1.0f), direction(0.0f)
{
	intensity = 1.0f;
}

Directional::Directional(const Directional& a) : Light(a), color(a.color), direction(a.direction)
{
	intensity = a.intensity;
}

Directional::~Directional(void)
{
}

Directional& Directional::operator=(const Directional& rhs) 
{
	if(this == &rhs) return *this;

	Light::operator=(rhs);
	
	intensity = rhs.intensity; 
	color = rhs.color;
	direction = rhs.direction;

	return *this;
}

Vector3d Directional::GetDirection(ShadeRec& sr)
{
	return direction;
}

RGBColor Directional::Radiance(ShadeRec& sr)
{
	return intensity * color;
}

//Update with real logic after this POS compiles
bool Directional::InShadow(const Ray& ray, const ShadeRec& sr) const
{
	return false;
}

