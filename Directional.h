#pragma once
#include "Light.h"

class Directional : public Light
{
public:
	Directional(void);
	Directional(const Directional&);
	~Directional(void);

	Directional& operator=(const Directional&);

	virtual Vector3d GetDirection(ShadeRec&);
	virtual RGBColor Radiance(ShadeRec&);

	void SetIntensity(float i) {intensity = i;}
	void SetColor(RGBColor c) {color = c;}
	void SetColor(float r, float g, float b) {color = RGBColor(r, g, b);}
	void SetColor(float c) {color = RGBColor(c);}
	void SetDirection(Vector3d l) {direction = l;}

	virtual bool InShadow(const Ray&, const ShadeRec&) const;

private:
	float intensity;
	RGBColor color;
	Vector3d direction;
};

