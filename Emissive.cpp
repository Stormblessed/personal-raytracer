#include "Emissive.h"

Emissive::Emissive(void)
{
}

Emissive::~Emissive(void)
{
}

RGBColor Emissive::GetEmissiveRadiance(ShadeRec& sr) const
{
	return intensity * color;
}

RGBColor Emissive::Shade(ShadeRec& sr)
{
	if(-sr.normal * sr.ray.direction > 0.0f)
	{
		return GetEmissiveRadiance(sr);
	}
	else
	{
		return black;
	}
}

RGBColor Emissive::AreaLightShade(ShadeRec& sr)
{
	if(-sr.normal * sr.ray.direction > 0.0f)
	{
		return GetEmissiveRadiance(sr);
	}
	else
	{
		return black;
	}
}
