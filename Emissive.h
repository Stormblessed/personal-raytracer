#pragma once
#include "Material.h"

class Emissive : public Material
{
public:
	Emissive(void);
	~Emissive(void);

	void SetIntensity(float i) {intensity = i;}
	void SetColor(RGBColor c) {color = c;}
	void SetColor(float r, float g, float b) {color = RGBColor(r, g, b);}
	void SetColor(float c) {color = RGBColor(c);}

	virtual RGBColor GetEmissiveRadiance(ShadeRec& sr) const;
	virtual RGBColor Shade(ShadeRec& sr);
	virtual RGBColor AreaLightShade(ShadeRec& sr);

private:
	float intensity;
	RGBColor color;
};

