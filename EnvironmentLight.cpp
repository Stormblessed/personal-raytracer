#include "EnvironmentLight.h"
#include "World.h"
#include "GeometricObject.h"

EnvironmentLight::EnvironmentLight(void) : Light(), object_ptr(NULL), material_ptr(NULL), sampler_ptr(NULL)
{
}

EnvironmentLight::~EnvironmentLight(void)
{
	if(object_ptr) 
	{
		delete object_ptr;
		object_ptr = NULL;
	}
	if(material_ptr) 
	{
		delete material_ptr;
		material_ptr = NULL;
	}
	if(sampler_ptr)
	{
		delete sampler_ptr;
		sampler_ptr = NULL;
	}
}

void EnvironmentLight::SetSampler(Sampler* s_ptr)
{
	if(sampler_ptr)
	{
		delete sampler_ptr;
		sampler_ptr = NULL;
	}
	sampler_ptr = s_ptr;
	sampler_ptr->MapSamplesToHemisphere(1);
}
	
Vector3d EnvironmentLight::GetDirection(ShadeRec& sr)
{
	w = sr.normal;
	v = Vector3d(0.0034f, 1.0f, 0.0071f) ^ w;
	v.Normalize();
	u = v ^ w;

	Point3d sp = sampler_ptr->SampleHemisphere();
	wi = sp.x * u + sp.y * v + sp.z * w;

	return wi;
}

RGBColor EnvironmentLight::Radiance(ShadeRec& sr)
{
	return material_ptr->GetEmissiveRadiance(sr);
}

bool EnvironmentLight::InShadow(const Ray& ray, const ShadeRec& sr) const
{
	float t;
	for(int i = 0; i < sr.world.objects.size(); i++)
	{
		if(sr.world.objects[i]->ShadowHit(ray, t)) return true;
	}
	return false;
}

float EnvironmentLight::PDF(const ShadeRec& sr) const
{
	//Use Infinitely large Sphere (concave)
	ShadeRec srNotConst = sr;
	return object_ptr->PDF(srNotConst);
}
