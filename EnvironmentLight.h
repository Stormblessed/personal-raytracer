#pragma once
#include "Light.h"
#include "Sampler.h"
#include "Material.h"

class GeometricObject;

class EnvironmentLight : public Light
{
public:
	EnvironmentLight(void);
	~EnvironmentLight(void);
	
	void SetObject(GeometricObject* obj) {object_ptr = obj;}
	void SetSampler(Sampler* sampler);
	void SetMaterial(Material* mat) {material_ptr = mat;}
	
	virtual Vector3d GetDirection(ShadeRec& sr);
	virtual RGBColor Radiance(ShadeRec& sr);

	bool InShadow(const Ray& ray, const ShadeRec& sr) const;
	virtual float PDF(const ShadeRec& sr) const;

private:
	GeometricObject* object_ptr;
	Sampler* sampler_ptr;
	Material* material_ptr;
	Vector3d u, v, w;
	Vector3d wi;
};

