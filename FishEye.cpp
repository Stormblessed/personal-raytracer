#include "FishEye.h"
#include "Constants.h"
#include <math.h>

FishEye::FishEye(void) : Camera()
{
	psiMax = 1.0f;
}

FishEye::FishEye(const FishEye& cam) : Camera(cam)	
{
	psiMax = 1.0f;
}

FishEye::~FishEye(void)
{
}

FishEye& FishEye::operator=(const FishEye& rhs) 
{ 	
	if(this == &rhs) return *this;
		
	Camera::operator= (rhs);

	psiMax = rhs.psiMax;

	return *this;
}

Vector3d FishEye::RayDirection(const Point2d& pixelPoint, const int hRes, const int vRes, const float pixelSize, float& rSquared) const
{
	Point2d normalPoint(2.0 / (pixelSize * hRes) * pixelPoint.x, 2.0 / (pixelSize * vRes) * pixelPoint.y);
	rSquared = normalPoint.x * normalPoint.x + normalPoint.y * normalPoint.y;

	if(rSquared <= 1.0f)
	{
		float r = sqrt(rSquared);
		float psi = r * psiMax * PI_ON_180;
		float sinPsi = sin(psi);
		float cosPsi = cos(psi);
		float sinAlpha = normalPoint.y / r;
		float cosAlpha = normalPoint.x / r;
		
		Vector3d dir = sinPsi * cosAlpha * u + sinPsi * sinAlpha * v - cosPsi * w;
		return dir;
	}
	else return Vector3d(0.0f);
}

void FishEye::Render(World& w)
{
	w.image.create(w.vp.hRes, w.vp.vRes, sf::Color::White);

	RGBColor radiance;
	ViewPlane vp(w.vp);
	Ray ray;
	int depth = 0;

	Point2d sampledPoint;
	Point2d pixelPoint;
	float rSquared;

	ray.origin = eye;

	for(int r = 0; r < vp.vRes; r++)
	{
		for(int c = 0; c < vp.hRes; c++)
		{
			radiance = black;

			for(int j = 0; j < vp.numSamples; j++)
			{
				sampledPoint = vp.sampler_ptr->SampleUnitSquare();
				pixelPoint.x = vp.pixelSize * (c - 0.5f * vp.hRes + sampledPoint.x);
				pixelPoint.y = vp.pixelSize * (r - 0.5f * vp.vRes + sampledPoint.y);
				
				ray.direction = RayDirection(pixelPoint, vp.hRes, vp.vRes, vp.pixelSize, rSquared);

				if(rSquared <= 1.0f) radiance += w.tracer_ptr->TraceRay(ray, depth);
			}

			radiance /= vp.numSamples;
			radiance *= exposureTime;
			sf::Color resultColor = sf::Color(radiance.r * 255, radiance.g * 255, radiance.b * 255, 255);

			int screenX = c;
			int screenY = vp.vRes - r - 1;

			w.image.setPixel(screenX, screenY, resultColor);
		}
	}
}

void FishEye::SetPsiMax(float psi) 
{
	psiMax = psi;
}	