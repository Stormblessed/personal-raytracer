#pragma once
#include "Camera.h"
#include "World.h"

class FishEye : public Camera
{
public:
	FishEye(void);
	FishEye(const FishEye&);	
	virtual ~FishEye(void);

	FishEye& operator= (const FishEye&);	

	Vector3d RayDirection(const Point2d&, const int, const int, const float, float&) const;
	virtual void Render(World&);

	void SetPsiMax(float);

private:
	float psiMax;
};

