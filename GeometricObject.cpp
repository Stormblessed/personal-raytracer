#include "GeometricObject.h"

GeometricObject::GeometricObject(void) : material_ptr(NULL)
{
}

GeometricObject::GeometricObject(const GeometricObject& object) 
{
	if(object.material_ptr) material_ptr = object.material_ptr->Clone(); 
	else material_ptr = NULL;
}

GeometricObject& GeometricObject::operator=(const GeometricObject& rhs) 
{
	if(this == &rhs) return *this;
			
	if(material_ptr)
	{
		delete material_ptr;
		material_ptr = NULL;
	}
	if(rhs.material_ptr) material_ptr = rhs.material_ptr->Clone();
	return *this;
}

GeometricObject::~GeometricObject(void) 
{	
	if(material_ptr) 
	{
		delete material_ptr;
		material_ptr = NULL;
	}
}

void GeometricObject::SetMaterial(Material* m) 
{
	material_ptr = m;
}

Material* GeometricObject::GetMaterial(void) const 
{
	return material_ptr;
}

Point3d GeometricObject::Sample(void)
{
	return Point3d(0.0f);
}

Normal GeometricObject::GetNormal(Point3d& p)
{
	return Normal();
}

float GeometricObject::PDF(ShadeRec& sr)
{
	return 0.0f;
}

BBox GeometricObject::GetBoundingBox(void)
{
	return BBox();
}

void GeometricObject::AddObject(GeometricObject* obj)
{
}