#pragma once
#include "Ray.h"
#include "RGBColor.h"
#include "ShadeRec.h"
#include "Constants.h"
#include "Material.h"
#include "BBox.h"

class GeometricObject
{
public:
	GeometricObject(void);
	GeometricObject(const GeometricObject&);
	~GeometricObject(void);

	virtual GeometricObject* Clone(void) const = 0;
	GeometricObject& operator=(const GeometricObject&); 

	virtual bool Hit(const Ray& ray, double& tMin, ShadeRec& sr) const = 0;
	virtual bool ShadowHit(const Ray& ray, float& tMin) const = 0;

	void SetColor(RGBColor c) {color = c;}
	void SetColor(float r, float g, float b) {color = RGBColor(r, g, b);}
	RGBColor GetColor() {return color;}

	Material* GetMaterial(void) const;
    virtual void SetMaterial(Material*);

	virtual Point3d Sample(void);
	virtual Normal GetNormal(Point3d& p);		
	virtual float PDF(ShadeRec& sr);

	virtual BBox GetBoundingBox(void);
	virtual void AddObject(GeometricObject* obj);

protected:
	mutable Material* material_ptr;
	RGBColor color;
};

