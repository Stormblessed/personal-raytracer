#include "GlossySpecular.h"

GlossySpecular::GlossySpecular(void) : BRDF(), specularColor(0.0f)
{
	reflectCoeff = 0.0f;
	exponent = 1.0f;
}

GlossySpecular::GlossySpecular(const GlossySpecular& gs) : BRDF(gs), specularColor(gs.specularColor)
{
	reflectCoeff = gs.reflectCoeff;
	exponent = gs.exponent;
}

GlossySpecular::~GlossySpecular(void)
{
}

GlossySpecular* GlossySpecular::Clone(void) const 
{
	return new GlossySpecular(*this);
}	

GlossySpecular& GlossySpecular::operator=(const GlossySpecular& rhs) 
{
	if(this == &rhs) return *this;
		
	BRDF::operator=(rhs);
	
	reflectCoeff = rhs.reflectCoeff; 
	specularColor = rhs.specularColor;
	exponent = rhs.exponent;
	
	return *this;
}

RGBColor GlossySpecular::F(const ShadeRec& sr, const Vector3d& wo, const Vector3d& wi) const 
{
	RGBColor radiance;
	float nDotWi = sr.normal * wi;
	Vector3d r(-wi + 2.0f * sr.normal * nDotWi);
	float rDotWo = r * wo;

	if(rDotWo > 0.0f) radiance = reflectCoeff * pow(rDotWo, exponent);
	return radiance;
}

RGBColor GlossySpecular::Rho(const ShadeRec& sr, const Vector3d& wo) const 
{
	return (reflectCoeff * specularColor);
}

void GlossySpecular::SetReflectCoeff(const float k) 
{
	reflectCoeff = k;
}

void GlossySpecular::SetSpecularColor(const RGBColor& c) 
{
	specularColor = c;
}

void GlossySpecular::SetSpecularColor(const float r, const float g, const float b) 
{
	specularColor.r = r; 
	specularColor.g = g; 
	specularColor.b = b;
}

void GlossySpecular::SetSpecularColor(const float c) 
{
	specularColor.r = c;
	specularColor.g = c; 
	specularColor.b = c;
}
