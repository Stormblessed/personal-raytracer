#pragma once
#include "BRDF.h"

class GlossySpecular : public BRDF
{
public:
	GlossySpecular(void);
	GlossySpecular(const GlossySpecular&);		
	~GlossySpecular(void);
		
	virtual GlossySpecular* Clone(void) const;
	GlossySpecular& operator= (const GlossySpecular& rhs);
		
	virtual RGBColor F(const ShadeRec&, const Vector3d&, const Vector3d&) const;
	virtual RGBColor Rho(const ShadeRec&, const Vector3d&) const;
							
	void SetReflectCoeff(const float);	
	void SetSpecularColor(const RGBColor&);
	void SetSpecularColor(const float, const float, const float);
	void SetSpecularColor(const float);
	void SetExponent(const float f) {exponent = f;}
					
private:
	float reflectCoeff;
	RGBColor specularColor;
	float exponent;
};