#include "Grid.h"

Grid::Grid(void) : Compound(), gridX(0), gridY(0), gridZ(0), reversedNormals(false)
{
}

Grid::Grid(const Grid& g) : Compound(), gridX(g.gridX), gridY(g.gridY), gridZ(g.gridZ), reversedNormals(g.reversedNormals)
{
}

Grid::~Grid(void)
{
}

Grid* Grid::Clone(void) const 
{
	return new Grid(*this);
}

Grid& Grid::operator=(const Grid& rhs)
{
	if(this == &rhs) return *this;

	Compound::operator=(rhs);

	gridX = rhs.gridX;
	gridY = rhs.gridY;
	gridZ = rhs.gridZ;
	reversedNormals = rhs.reversedNormals;
		
	return *this;
}

BBox Grid::GetBoundingBox(void)
{
	return bbox;
}

Point3d Grid::FindMinBounds(void)
{
	BBox objBox;
	Point3d p0(kHugeValue);

	for(int i = 0; i < objects.size(); i++)
	{
		objBox = objects[i]->GetBoundingBox();

		if(objBox.x0 < p0.x)
		{
			p0.x = objBox.x0;
		}
		if(objBox.y0 < p0.y)
		{
			p0.y = objBox.y0;
		}
		if(objBox.z0 < p0.z)
		{
			p0.z = objBox.z0;
		}
	}

	p0.x -= kEpsilon;
	p0.y -= kEpsilon;
	p0.z -= kEpsilon;

	return p0;
}

Point3d Grid::FindMaxBounds(void)
{
	BBox objBox;
	Point3d p1(-kHugeValue);

	for(int i = 0; i < objects.size(); i++)
	{
		objBox = objects[i]->GetBoundingBox();

		if(objBox.x1 > p1.x)
		{
			p1.x = objBox.x1;
		}
		if(objBox.y1 > p1.y)
		{
			p1.y = objBox.y1;
		}
		if(objBox.z1 > p1.z)
		{
			p1.z = objBox.z1;
		}
	}

	p1.x += kEpsilon;
	p1.y += kEpsilon;
	p1.z += kEpsilon;

	return p1;
}

void Grid::SetupCells(void)
{
	Point3d minPoint = FindMinBounds();
	Point3d maxPoint = FindMaxBounds();

	bbox = BBox(minPoint, maxPoint);

	int numObjects = GetNumObjects();

	double boundsX = maxPoint.x - minPoint.x;
	double boundsY = maxPoint.y - minPoint.y;
	double boundsZ = maxPoint.z - minPoint.z;

	double gridMultiplier = 2.0f;
	double size = pow((boundsX * boundsY * boundsZ) / numObjects, 0.3333333f);
	gridX = gridMultiplier * boundsX / size + 1;
	gridY = gridMultiplier * boundsY / size + 1;
	gridZ = gridMultiplier * boundsZ / size + 1;

	int numCells = gridX * gridY * gridZ;
	cells.reserve(numObjects);

	std::vector<int> counts;
	for(int i = 0; i < numCells; i++)
	{
		cells.push_back(NULL);
		counts.push_back(0);
	}

	BBox objBbox;
	int index;

	for(int i = 0; i < numObjects; i++)
	{
		objBbox = objects[i]->GetBoundingBox();

		int minIndexX = Clamp((objBbox.x0 - minPoint.x) * gridX / (maxPoint.x - minPoint.x), 0.0f, gridX - 1.0f);
		int minIndexY = Clamp((objBbox.y0 - minPoint.y) * gridY / (maxPoint.y - minPoint.y), 0.0f, gridY - 1.0f);
		int minIndexZ = Clamp((objBbox.z0 - minPoint.z) * gridZ / (maxPoint.z - minPoint.z), 0.0f, gridZ - 1.0f);

		int maxIndexX = Clamp((objBbox.x1 - minPoint.x) * gridX / (maxPoint.x - minPoint.x), 0.0f, gridX - 1.0f);
		int maxIndexY = Clamp((objBbox.y1 - minPoint.y) * gridY / (maxPoint.y - minPoint.y), 0.0f, gridY - 1.0f);
		int maxIndexZ = Clamp((objBbox.z1 - minPoint.z) * gridZ / (maxPoint.z - minPoint.z), 0.0f, gridZ - 1.0f);

		for(int z = minIndexZ; z <= maxIndexZ; z++)
		{
			for(int y = minIndexY; y <= maxIndexY; y++)
			{
				for(int x = minIndexX; x <= maxIndexX; x++)
				{
					index = x + gridX * y + gridX * gridY * z;

					if(counts[index] == 0)
					{
						cells[index] = objects[i];
						counts[index] += 1;
					}
					else
					{
						if(counts[index] == 1)
						{
							Compound* compound_ptr = new Compound();
							compound_ptr->AddObject(cells[index]);
							compound_ptr->AddObject(objects[i]);
							cells[index] = compound_ptr;
							counts[index] += 1;
						}
						else
						{
							cells[index]->AddObject(objects[i]);
							counts[index] += 1;
						}
					}
				}
			}
		}
	}

	objects.erase(objects.begin(), objects.end());

	/*Debugging counts
	int numZeroes = 0;
	int numOnes = 0;
	int numTwos = 0;
	int numThrees = 0;
	int numGreaters = 0;

	for(int i = 0; i < numCells; i++)
	{
		if(counts[i] == 0) numZeroes++;
		else if(counts[i] == 1) numOnes++;
		else if(counts[i] == 2) numTwos++;
		else if(counts[i] == 3) numThrees++;
		else if(counts[i] > 3) numGreaters++;
	}

	std::cout << "NumCells: " << numCells << std::endl;
	std::cout << "NumZeroes: " << numZeroes << std::endl;
	std::cout << "NumOnes: " << numOnes << std::endl;
	std::cout << "NumTwos: " << numTwos << std::endl;
	std::cout << "NumThrees: " << numThrees << std::endl;
	std::cout << "NumGreaters: " << numGreaters << std::endl;
	*/

	counts.erase(counts.begin(), counts.end());
}

void Grid::ReverseMeshNormals(void) 
{   
	reversedNormals = true;
}

void Grid::StoreMaterial(Material* mat, const int index) 
{
	objects[index]->SetMaterial(mat); 
}

bool Grid::Hit(const Ray& ray, double& tMin, ShadeRec& sr) const
{
	double txMin, tyMin, tzMin;
	double txMax, tyMax, tzMax;

	double a = 1.0f / ray.direction.x;
	if(a >= 0)
	{
		txMin = (bbox.x0 - ray.origin.x) * a;
		txMax = (bbox.x1 - ray.origin.x) * a;
	}
	else
	{
		txMin = (bbox.x1 - ray.origin.x) * a;
		txMax = (bbox.x0 - ray.origin.x) * a;
	}

	double b = 1.0f / ray.direction.y;
	if(b >= 0)
	{
		tyMin = (bbox.y0 - ray.origin.y) * b;
		tyMax = (bbox.y1 - ray.origin.y) * b;
	}
	else
	{
		tyMin = (bbox.y1 - ray.origin.y) * b;
		tyMax = (bbox.y0 - ray.origin.y) * b;
	}

	double c = 1.0f / ray.direction.z;
	if(c >= 0)
	{
		tzMin = (bbox.z0 - ray.origin.z) * c;
		tzMax = (bbox.z1 - ray.origin.z) * c;
	}
	else
	{
		tzMin = (bbox.z1 - ray.origin.z) * c;
		tzMax = (bbox.z0 - ray.origin.z) * c;
	}

	double t0, t1;

	if(txMin > tyMin) t0 = txMin;
	else t0 = tyMin;
	if(tzMin > t0) t0 = tzMin;

	if(txMax < tyMax) t1 = txMax;
	else t1 = tyMax;
	if(tzMax < t1) t1 = tzMax;

	if(t0 > t1) return false;

	int initialX, initialY, initialZ;
	if(bbox.Contains(ray.origin))
	{
		initialX = Clamp((ray.origin.x - bbox.x0) * gridX / (bbox.x1 - bbox.x0), 0.0f, gridX - 1.0f);
		initialY = Clamp((ray.origin.y - bbox.y0) * gridY / (bbox.y1 - bbox.y0), 0.0f, gridY - 1.0f);
		initialZ = Clamp((ray.origin.z - bbox.z0) * gridZ / (bbox.z1 - bbox.z0), 0.0f, gridZ - 1.0f);
	}
	else
	{
		Point3d entryPoint = ray.origin + t0 * ray.direction;
		initialX = Clamp((entryPoint.x - bbox.x0) * gridX / (bbox.x1 - bbox.x0), 0.0f, gridX - 1.0f);
		initialY = Clamp((entryPoint.y - bbox.y0) * gridY / (bbox.y1 - bbox.y0), 0.0f, gridY - 1.0f);
		initialZ = Clamp((entryPoint.z - bbox.z0) * gridZ / (bbox.z1 - bbox.z0), 0.0f, gridZ - 1.0f);
	}

	double dtX = (txMax - txMin) / gridX;
	double dtY = (tyMax - tyMin) / gridY;
	double dtZ = (tzMax - tzMin) / gridZ;

	double txNext, tyNext, tzNext;
	int ixStep, iyStep, izStep;
	int ixStop, iyStop, izStop;

	if(ray.direction.x > 0.0f)
	{
		txNext = txMin + (initialX + 1) * dtX;
		ixStep = 1;
		ixStop = gridX;
	}
	else
	{
		txNext = txMin + (gridX - initialX) * dtX;
		ixStep = -1;
		ixStop = -1;
	}
	if(ray.direction.x == 0.0f)
	{
		txNext = kHugeValue;
		ixStep = -1;
		ixStop = -1;
	}

	if(ray.direction.y > 0.0f)
	{
		tyNext = tyMin + (initialY + 1) * dtY;
		iyStep = 1;
		iyStop = gridY;
	}
	else
	{
		tyNext = tyMin + (gridY - initialY) * dtY;
		iyStep = -1;
		iyStop = -1;
	}
	if(ray.direction.y == 0.0f)
	{
		tyNext = kHugeValue;
		iyStep = -1;
		iyStop = -1;
	}

	if(ray.direction.z > 0.0f)
	{
		tzNext = tzMin + (initialZ + 1) * dtZ;
		izStep = 1;
		izStop = gridZ;
	}
	else
	{
		tzNext = tzMin + (gridZ - initialZ) * dtZ;
		izStep = -1;
		izStop = -1;
	}
	if(ray.direction.z == 0.0f)
	{
		tzNext = kHugeValue;
		izStep = -1;
		izStop = -1;
	}

	//this code is so verbose.

	while(true) 
	{	
		GeometricObject* object_ptr = cells[initialX + gridX * initialY + gridX * gridY * initialZ];
		
		if(txNext < tyNext && txNext < tzNext) 
		{
			if(object_ptr && object_ptr->Hit(ray, tMin, sr) && tMin < txNext) 
			{
				material_ptr = object_ptr->GetMaterial();
				return true;
			}
			
			txNext += dtX;
			initialX += ixStep;
						
			if(initialX == ixStop) return false;
		} 
		else 
		{ 	
			if(tyNext < tzNext) 
			{
				if(object_ptr && object_ptr->Hit(ray, tMin, sr) && tMin < tyNext) 
				{
					material_ptr = object_ptr->GetMaterial();
					return true;
				}
				
				tyNext += dtY;
				initialY += iyStep;
								
				if(initialY == iyStop) return false;
		 	}
		 	else 
			{		
				if(object_ptr && object_ptr->Hit(ray, tMin, sr) && tMin < tzNext) 
				{
					material_ptr = object_ptr->GetMaterial();
					return true;
				}
				
				tzNext += dtZ;
				initialZ += izStep;
								
				if(initialZ == izStop) return false;
		 	}
		}
	}
}

bool Grid::ShadowHit(const Ray& ray, float& tMin) const
{
	double txMin, tyMin, tzMin;
	double txMax, tyMax, tzMax;

	double a = 1.0f / ray.direction.x;
	if(a >= 0.0f)
	{
		txMin = (bbox.x0 - ray.origin.x) * a;
		txMax = (bbox.x1 - ray.origin.x) * a;
	}
	else
	{
		txMin = (bbox.x1 - ray.origin.x) * a;
		txMax = (bbox.x0 - ray.origin.x) * a;
	}

	double b = 1.0f / ray.direction.y;
	if(b >= 0)
	{
		tyMin = (bbox.y0 - ray.origin.y) * b;
		tyMax = (bbox.y1 - ray.origin.y) * b;
	}
	else
	{
		tyMin = (bbox.y1 - ray.origin.y) * b;
		tyMax = (bbox.y0 - ray.origin.y) * b;
	}

	double c = 1.0f / ray.direction.z;
	if(c >= 0)
	{
		tzMin = (bbox.z0 - ray.origin.z) * c;
		tzMax = (bbox.z1 - ray.origin.z) * c;
	}
	else
	{
		tzMin = (bbox.z1 - ray.origin.z) * c;
		tzMax = (bbox.z0 - ray.origin.z) * c;
	}

	double t0, t1;

	if(txMin > tyMin) t0 = txMin;
	else t0 = tyMin;
	if(tzMin > t0) t0 = tzMin;

	if(txMax < tyMax) t1 = txMax;
	else t1 = tyMax;
	if(tzMax < t1) t1 = tzMax;

	if(t0 > t1) return false;

	int initialX, initialY, initialZ;
	if(bbox.Contains(ray.origin))
	{
		initialX = Clamp((ray.origin.x - bbox.x0) * gridX / (bbox.x1 - bbox.x0), 0.0f, gridX - 1.0f);
		initialY = Clamp((ray.origin.y - bbox.y0) * gridY / (bbox.y1 - bbox.y0), 0.0f, gridY - 1.0f);
		initialZ = Clamp((ray.origin.z - bbox.z0) * gridZ / (bbox.z1 - bbox.z0), 0.0f, gridZ - 1.0f);
	}
	else
	{
		Point3d entryPoint = ray.origin + t0 * ray.direction;
		initialX = Clamp((entryPoint.x - bbox.x0) * gridX / (bbox.x1 - bbox.x0), 0.0f, gridX - 1.0f);
		initialY = Clamp((entryPoint.y - bbox.y0) * gridY / (bbox.y1 - bbox.y0), 0.0f, gridY - 1.0f);
		initialZ = Clamp((entryPoint.z - bbox.z0) * gridZ / (bbox.z1 - bbox.z0), 0.0f, gridZ - 1.0f);
	}

	double dtX = (txMax - txMin) / gridX;
	double dtY = (tyMax - tyMin) / gridY;
	double dtZ = (tzMax - tzMin) / gridZ;

	double txNext, tyNext, tzNext;
	int ixStep, iyStep, izStep;
	int ixStop, iyStop, izStop;

	if(ray.direction.x > 0.0f)
	{
		txNext = txMin + (initialX + 1) * dtX;
		ixStep = 1;
		ixStop = gridX;
	}
	else
	{
		txNext = txMin + (gridX - initialX) * dtX;
		ixStep = -1;
		ixStop = -1;
	}
	if(ray.direction.x == 0.0f)
	{
		txNext = kHugeValue;
		ixStep = -1;
		ixStop = -1;
	}

	if(ray.direction.y > 0.0f)
	{
		tyNext = tyMin + (initialY + 1) * dtY;
		iyStep = 1;
		iyStop = gridY;
	}
	else
	{
		tyNext = tyMin + (gridY - initialY) * dtY;
		iyStep = -1;
		iyStop = -1;
	}
	if(ray.direction.y == 0.0f)
	{
		tyNext = kHugeValue;
		iyStep = -1;
		iyStop = -1;
	}

	if(ray.direction.z > 0.0f)
	{
		tzNext = tzMin + (initialZ + 1) * dtZ;
		izStep = 1;
		izStop = gridZ;
	}
	else
	{
		tzNext = tzMin + (gridZ - initialZ) * dtZ;
		izStep = -1;
		izStop = -1;
	}
	if(ray.direction.z == 0.0f)
	{
		tzNext = kHugeValue;
		izStep = -1;
		izStop = -1;
	}

	//this code is so verbose pt 2

	while(true) 
	{	
		GeometricObject* object_ptr = cells[initialX + gridX * initialY + gridX * gridY * initialZ];
		
		if(txNext < tyNext && txNext < tzNext) 
		{
			if(object_ptr && object_ptr->ShadowHit(ray, tMin) && tMin < txNext) 
			{
				material_ptr = object_ptr->GetMaterial();
				return true;
			}
			
			txNext += dtX;
			initialX += ixStep;
						
			if(initialX == ixStop) return false;
		} 
		else 
		{ 	
			if(tyNext < tzNext) 
			{
				if(object_ptr && object_ptr->ShadowHit(ray, tMin) && tMin < tyNext) 
				{
					material_ptr = object_ptr->GetMaterial();
					return true;
				}
				
				tyNext += dtY;
				initialY += iyStep;
								
				if(initialY == iyStop) return false;
		 	}
		 	else 
			{		
				if(object_ptr && object_ptr->ShadowHit(ray, tMin) && tMin < tzNext) 
				{
					material_ptr = object_ptr->GetMaterial();
					return true;
				}
				
				tzNext += dtZ;
				initialZ += izStep;
								
				if(initialZ == izStop) return false;
		 	}
		}
	}
}