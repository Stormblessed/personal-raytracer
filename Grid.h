#pragma once
#include "Compound.h"
#include "MathHelper.h"
#include <iostream>

class Grid: public Compound 
{										  	
public:
	Grid(void); 
	//Grid(Mesh* _mesh_ptr);
	Grid(const Grid& g);
	virtual ~Grid(void);   			

	virtual Grid* Clone(void) const;
	Grid& operator=(const Grid& rhs);

	virtual BBox GetBoundingBox(void);

	//void ReadFlatTriangles(char* path);
	//void ReadSmoothTriangles(char* path);

	//void TessellateFlatSphere(const int horiSteps, const int vertSteps);
	//void TesselateSmoothSphere(const int horiSteps, const int vertSteps);

	virtual bool Hit(const Ray& ray, double& tMin, ShadeRec& sr) const;
	virtual bool ShadowHit(const Ray& ray, float& tMin) const;	

	void SetupCells(void);
	void ReverseMeshNormals(void);

	void StoreMaterial(Material* mat, const int index); 							

private: 

	std::vector<GeometricObject*> cells;
	int gridX, gridY, gridZ;
	BBox bbox;
	//Mesh* mesh_ptr;
	bool reversedNormals;

	Point3d FindMinBounds(void);
	Point3d FindMaxBounds(void);

	//void LoadFromObj(char* path, const int triangleType);
	//void ComputeMeshNormals(void);	
};

