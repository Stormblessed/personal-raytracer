#include "Hammersly.h"

Hammersly::Hammersly(const int num) : Sampler(num) 
{
	GenerateSamples();
}

Hammersly::Hammersly(const Hammersly& r)	: Sampler(r) 
{
	GenerateSamples();
}

Hammersly::~Hammersly(void)
{
}

Hammersly& Hammersly::operator=(const Hammersly& rhs) 
{
	if(this == &rhs) return *this;
		
	Sampler::operator=(rhs);

	return *this;
}

Hammersly* Hammersly::Clone(void) const 
{
	return (new Hammersly(*this));
}

double Hammersly::Phi(int j) 
{
	double x = 0.0;
	double f = 0.5; 
	
	while(j) 
	{
		x += f * (double) (j % 2);
		j /= 2;
		f *= 0.5; 
	}
	
	return x;
}

void Hammersly::GenerateSamples(void) {

	for (int p = 0; p < numSets; p++)
	{
		for (int j = 0; j < numSamples; j++) 
		{
			Point2d pv((float) j / (float) numSamples, Phi(j));
			samples.push_back(pv);
		}
	}
}
