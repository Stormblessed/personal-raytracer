#pragma once
#include "Sampler.h"

class Hammersly : public Sampler
{
public:
	Hammersly(void);
	Hammersly(const int);					
	Hammersly(const Hammersly&);	
	virtual ~Hammersly(void);

	Hammersly& operator=(const Hammersly& rhs);

	virtual Hammersly*	Clone(void) const;

	double Phi(int);

private:
	virtual void GenerateSamples(void);
};

