#include "Instance.h"
#include "Constants.h"

Matrix Instance::forwardMatrix;

Instance::Instance(void) : GeometricObject(), object_ptr(NULL), invMatrix(), boundingBox(), shouldTransformTexture(true)
{
	forwardMatrix.SetIdentity();
}

Instance::Instance(GeometricObject* obj_ptr) : GeometricObject(), object_ptr(obj_ptr), invMatrix(), boundingBox(), shouldTransformTexture(true)
{
	forwardMatrix.SetIdentity();
}	

Instance::Instance(const Instance& instance) : GeometricObject(instance), invMatrix(instance.invMatrix), shouldTransformTexture(instance.shouldTransformTexture)
{
	if(instance.object_ptr) object_ptr = instance.object_ptr->Clone(); 
	else object_ptr = NULL;
}

Instance::~Instance(void) 
{
	if(object_ptr) 
	{
		delete object_ptr;
		object_ptr = NULL;
	}
}

Instance* Instance::Clone(void) const 
{
	return new Instance(*this);
}

Instance& Instance::operator=(const Instance& rhs) 
{
	if(this == &rhs) return *this;

	GeometricObject::operator=(rhs);
		
	if(object_ptr) 
	{
		delete object_ptr;
		object_ptr = NULL;
	}
	
	if(rhs.object_ptr) object_ptr = rhs.object_ptr->Clone();
	else object_ptr = NULL;
	
	invMatrix = rhs.invMatrix;
	boundingBox	= rhs.boundingBox;
	shouldTransformTexture = rhs.shouldTransformTexture;
	
	return *this;
}

void Instance::SetObject(GeometricObject* obj_ptr) 
{
	object_ptr = obj_ptr;
}

BBox Instance::GetBoundingBox(void) 
{
	return boundingBox;
}
																
Material* Instance::GetMaterial(void) const 
{
	return material_ptr;
}

void Instance::SetMaterial(Material* m_ptr) 
{
	material_ptr = m_ptr;
}

void Instance::TransformTexture(const bool transform) 
{
	shouldTransformTexture = transform;
}	

void Instance::ComputeBoundingBox(void) 
{	
	BBox object_bbox = object_ptr->GetBoundingBox();
	
	
	// Now apply the affine transformations to the box.
	// We must apply the transformations to all 8 vertices of the orginal box
	// and then work out the new minimum and maximum values
	
	// Construct the eight vertices as 3D points:
	Point3d v[8];
	
	v[0].x = object_bbox.x0; 
	v[0].y = object_bbox.y0; 
	v[0].z = object_bbox.z0;

	v[1].x = object_bbox.x1; 
	v[1].y = object_bbox.y0; 
	v[1].z = object_bbox.z0;

	v[2].x = object_bbox.x1; 
	v[2].y = object_bbox.y1; 
	v[2].z = object_bbox.z0;

	v[3].x = object_bbox.x0; 
	v[3].y = object_bbox.y1; 
	v[3].z = object_bbox.z0;
	

	v[4].x = object_bbox.x0; 
	v[4].y = object_bbox.y0; 
	v[4].z = object_bbox.z1;

	v[5].x = object_bbox.x1; 
	v[5].y = object_bbox.y0; 
	v[5].z = object_bbox.z1;

	v[6].x = object_bbox.x1; 
	v[6].y = object_bbox.y1; 
	v[6].z = object_bbox.z1;

	v[7].x = object_bbox.x0; 
	v[7].y = object_bbox.y1; 
	v[7].z = object_bbox.z1;
	
	// Transform these using the forward matrix
	v[0] = forwardMatrix * v[0];
	v[1] = forwardMatrix * v[1];
	v[2] = forwardMatrix * v[2];
	v[3] = forwardMatrix * v[3];
	v[4] = forwardMatrix * v[4];
	v[5] = forwardMatrix * v[5];
	v[6] = forwardMatrix * v[6];
	v[7] = forwardMatrix * v[7];

	
	// Since forward_matrix is a static variable, we must now set it to the unit matrix
	// This sets it up correctly for the next instance object
	
	forwardMatrix.SetIdentity();
	
	// Compute the minimum values
	float x0 = kHugeValue;
	float y0 = kHugeValue;
	float z0 = kHugeValue;
		
	for(int j = 0; j <= 7; j++)  
	{
		if(v[j].x < x0) 
		{
			x0 = v[j].x;
		}
	}
		
	for(int j = 0; j <= 7; j++) 
	{
		if(v[j].y < y0) 
		{
			y0 = v[j].y;
		}
	}
		
	for (int j = 0; j <= 7; j++) 
	{
		if(v[j].z < z0)
		{
			z0 = v[j].z;
		}
	}
	
	// Compute the minimum values
	float x1 = -kHugeValue;
	float y1 = -kHugeValue;
	float z1 = -kHugeValue;   
	
	for(int j = 0; j <= 7; j++) 
	{
		if(v[j].x > x1) 
		{
			x1 = v[j].x;
		}
	}
		
	for (int j = 0; j <= 7; j++) 
	{
		if(v[j].y > y1)
		{
			y1 = v[j].y;
		}
	}
		
	for(int j = 0; j <= 7; j++) 
	{
		if(v[j].z > z1)
		{
			z1 = v[j].z;
		}
	}
	
	// Assign values to the bounding box
	boundingBox.x0 = x0;
	boundingBox.y0 = y0;
	boundingBox.z0 = z0;
	boundingBox.x1 = x1;
	boundingBox.y1 = y1;
	boundingBox.z1 = z1;	
}

bool Instance::Hit(const Ray& ray, double& tMin, ShadeRec& sr) const 
{
	Ray inverseRay(ray);  
	inverseRay.origin = invMatrix * inverseRay.origin;   
	inverseRay.direction = invMatrix * inverseRay.direction;
				
	if(object_ptr->Hit(inverseRay, tMin, sr)) 
	{
		sr.normal = invMatrix * sr.normal;
		sr.normal.Normalize();				
						
		if(object_ptr->GetMaterial())
		{
			material_ptr = object_ptr->GetMaterial();
		}
		if(!shouldTransformTexture)
		{
			sr.localHitPoint = ray.origin + tMin * ray.direction;  		
		}
		return true;
	} 
	return false;   
}

bool Instance::ShadowHit(const Ray& ray, float& tMin) const 
{
	Ray inverseRay(ray);  
	inverseRay.origin = invMatrix * inverseRay.origin;   
	inverseRay.direction = invMatrix * inverseRay.direction;
				
	if(object_ptr->ShadowHit(inverseRay, tMin)) 
	{						
		if(object_ptr->GetMaterial())
		{
			material_ptr = object_ptr->GetMaterial();
		}
		return true;
	} 
	return false;  
}

void Instance::Scale(const Vector3d& s) 
{
	Matrix invScalingMatrix;
	
	invScalingMatrix.m[0][0] = 1.0 / s.x;
	invScalingMatrix.m[1][1] = 1.0 / s.y;
	invScalingMatrix.m[2][2] = 1.0 / s.z;

	invMatrix = invMatrix * invScalingMatrix;			

	Matrix scalingMatrix;
	
	scalingMatrix.m[0][0] = s.x;
	scalingMatrix.m[1][1] = s.y;
	scalingMatrix.m[2][2] = s.z;
	
	forwardMatrix = scalingMatrix * forwardMatrix; 		
}


void Instance::Scale(const double a, const double b, const double c) 
{
	Matrix invScalingMatrix;
	
	invScalingMatrix.m[0][0] = 1.0 / a;
	invScalingMatrix.m[1][1] = 1.0 / b;
	invScalingMatrix.m[2][2] = 1.0 / c;

	invMatrix = invMatrix * invScalingMatrix;			

	Matrix scalingMatrix;
	
	scalingMatrix.m[0][0] = a;
	scalingMatrix.m[1][1] = b;
	scalingMatrix.m[2][2] = c;
	
	forwardMatrix = scalingMatrix * forwardMatrix; 		
}

void Instance::Translate(const Vector3d& translation) 
{
	Matrix inverseTranslationMatrix;

	inverseTranslationMatrix.m[0][3] = -translation.x;
	inverseTranslationMatrix.m[1][3] = -translation.y;
	inverseTranslationMatrix.m[2][3] = -translation.z;
					
	invMatrix = invMatrix * inverseTranslationMatrix;
	
	Matrix translationMatrix;
	
	translationMatrix.m[0][3] = translation.x;
	translationMatrix.m[1][3] = translation.y;
	translationMatrix.m[2][3] = translation.z;
	
	forwardMatrix = translationMatrix * forwardMatrix; 
}

void Instance::Translate(const double dx, const double dy, const double dz) 
{
	Matrix inverseTranslationMatrix;

	inverseTranslationMatrix.m[0][3] = -dx;
	inverseTranslationMatrix.m[1][3] = -dy;
	inverseTranslationMatrix.m[2][3] = -dz;
					
	invMatrix = invMatrix * inverseTranslationMatrix;
	
	Matrix translationMatrix;
	
	translationMatrix.m[0][3] = dx;
	translationMatrix.m[1][3] = dy;
	translationMatrix.m[2][3] = dz;
	
	forwardMatrix = translationMatrix * forwardMatrix; 
}

void Instance::RotateX(const double theta) 
{
	double sinTheta = sin(theta * PI_ON_180);
	double cosTheta = cos(theta * PI_ON_180);
	
	Matrix invXRotationMatrix;
	
	invXRotationMatrix.m[1][1] = cosTheta;
	invXRotationMatrix.m[1][2] = sinTheta;   	
	invXRotationMatrix.m[2][1] = -sinTheta;  
	invXRotationMatrix.m[2][2] = cosTheta;		
					
	invMatrix = invMatrix * invXRotationMatrix;	   	
	
	Matrix xRotationMatrix;
	
	xRotationMatrix.m[1][1] = cosTheta;
	xRotationMatrix.m[1][2] = -sinTheta;
	xRotationMatrix.m[2][1] = sinTheta;
	xRotationMatrix.m[2][2] = cosTheta;
				
	forwardMatrix = xRotationMatrix * forwardMatrix; 
}

void Instance::RotateY(const double theta) 
{
	double sinTheta = sin(theta * PI_ON_180);
	double cosTheta = cos(theta * PI_ON_180);

	Matrix invYRotationMatrix;
	
	invYRotationMatrix.m[0][0] = cosTheta;   
	invYRotationMatrix.m[0][2] = -sinTheta;  
	invYRotationMatrix.m[2][0] = sinTheta;
	invYRotationMatrix.m[2][2] = cosTheta;		
					
	invMatrix = invMatrix * invYRotationMatrix;	   	
	
	Matrix yRotationMatrix;
	
	yRotationMatrix.m[0][0] = cosTheta;
	yRotationMatrix.m[0][2] = sinTheta;
	yRotationMatrix.m[2][0] = -sinTheta;
	yRotationMatrix.m[2][2] = cosTheta;
				
	forwardMatrix = yRotationMatrix * forwardMatrix; 
}

void Instance::RotateZ(const double theta) 
{
	double sinTheta = sin(theta * PI_ON_180);
	double cosTheta = cos(theta * PI_ON_180);

	Matrix invZRotationMatrix;

	invZRotationMatrix.m[0][0] = cosTheta;
	invZRotationMatrix.m[0][1] = sinTheta;   	
	invZRotationMatrix.m[1][0] = -sinTheta;  
	invZRotationMatrix.m[1][1] = cosTheta;	
					
	invMatrix = invMatrix * invZRotationMatrix;
	
	Matrix zRotationMatrix;
	
	zRotationMatrix.m[0][0] = cosTheta;
	zRotationMatrix.m[0][1] = -sinTheta;
	zRotationMatrix.m[1][0] = sinTheta;
	zRotationMatrix.m[1][1] = cosTheta;
				
	forwardMatrix = zRotationMatrix * forwardMatrix; 
}

void Instance::Shear(const Matrix& s) 
{
	Matrix inverseShearingMatrix;
	
	// discriminant
	double discriminant = 1.0 - s.m[1][0] * s.m[0][1] - s.m[2][0] * s.m[0][2] - s.m[2][1] * s.m[1][2] + s.m[1][0] * s.m[2][1] * s.m[0][2] + s.m[2][0] * s.m[0][1] * s.m[2][1];
					
	// diagonals
	inverseShearingMatrix.m[0][0] = 1.0 - s.m[2][1] * s.m[1][2];
	inverseShearingMatrix.m[1][1] = 1.0 - s.m[2][0] * s.m[0][2];
	inverseShearingMatrix.m[2][2] = 1.0 - s.m[1][0] * s.m[0][1];
	inverseShearingMatrix.m[3][3] = discriminant;
	
	// first row
	inverseShearingMatrix.m[0][1] = -s.m[1][0] + s.m[2][0] * s.m[1][2];
	inverseShearingMatrix.m[0][2] = -s.m[2][0] + s.m[1][0] * s.m[2][1];
	
	// second row
	inverseShearingMatrix.m[1][0] = -s.m[0][1] + s.m[2][1] * s.m[0][2];
	inverseShearingMatrix.m[1][2] = -s.m[2][1] + s.m[2][0] * s.m[0][1];
	
	// third row
	inverseShearingMatrix.m[2][0] = -s.m[0][2] + s.m[0][1] * s.m[1][2];
	inverseShearingMatrix.m[2][1] = -s.m[1][2] + s.m[1][0] * s.m[0][2] ;
	
	// divide by discriminant
	inverseShearingMatrix = inverseShearingMatrix / discriminant;
	
	invMatrix = invMatrix * inverseShearingMatrix;	
	
	forwardMatrix = s * forwardMatrix; 
}