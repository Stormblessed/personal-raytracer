#pragma once
#include "GeometricObject.h"
#include "Matrix.h"

class Instance: public GeometricObject 
{	
public:	
	Instance(void);   									
	Instance(GeometricObject* obj_ptr);					
	Instance(const Instance& instance); 			
	virtual	~Instance(void); 
	
	virtual Instance* Clone(void) const;	
	Instance& operator=(const Instance& rhs);

	void SetObject(GeometricObject* obj_ptr);
	virtual Material* GetMaterial(void) const;
	virtual void SetMaterial(Material* materialPtr);

	void TransformTexture(const bool transform);
	virtual void ComputeBoundingBox(void);
	virtual BBox GetBoundingBox(void);						
	

	virtual bool Hit(const Ray& ray, double& tMin, ShadeRec& sr) const;
	virtual bool ShadowHit(const Ray& ray, float& tMin) const; 

	void Translate(const Vector3d& trans);
	void Translate(const double dx, const double dy, const double dz);	
	void Scale(const Vector3d& s);
	void Scale(const double a, const double b, const double c);
	virtual void RotateX(const double r);
	virtual void RotateY(const double r);
	virtual void RotateZ(const double r);
	void Shear(const Matrix& m);

private:
	GeometricObject* object_ptr;
	Matrix invMatrix;	
	static Matrix forwardMatrix; 
	BBox boundingBox;
	bool shouldTransformTexture;
};