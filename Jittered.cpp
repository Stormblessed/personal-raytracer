#include "Jittered.h"

Jittered::Jittered(void) : Sampler()
{
}

Jittered::Jittered(const int ns) : Sampler(ns) 
{
	GenerateSamples();
}

Jittered::Jittered(const int ns, const int m) : Sampler(ns, m) 
{
	GenerateSamples();
}

Jittered::Jittered(const Jittered& js) : Sampler(js) 
{
	GenerateSamples();
}

Jittered::~Jittered(void)
{
}

Jittered& Jittered::operator=(const Jittered& rhs) 
{
	if(this == &rhs) return *this;
		
	Sampler::operator= (rhs);

	return *this;
}

Jittered* Jittered::Clone(void) const 
{
	return new Jittered(*this);
}

void Jittered::GenerateSamples(void) 
{	
	int n = (int) sqrt((float)numSamples); 
	
	for (int p = 0; p < numSets; p++)
	{
		for (int j = 0; j < n; j++)
		{
			for (int k = 0; k < n; k++) 
			{
				Point2d samplePoint((k + RandFloat()) / n, (j + RandFloat()) / n);				
				samples.push_back(samplePoint);
			}
		}
	}
}