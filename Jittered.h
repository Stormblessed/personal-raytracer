#pragma once
#include "Sampler.h"

class Jittered : public Sampler
{
public:
	Jittered(void);
	Jittered(const int);					
	Jittered(const int, const int);	
	Jittered(const Jittered&);	
	virtual ~Jittered(void);

	Jittered& operator=(const Jittered&);

	virtual Jittered* Clone(void) const;

private:
	virtual void GenerateSamples(void);
};