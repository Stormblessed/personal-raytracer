#include "Lambertian.h"

Lambertian::Lambertian(void) : BRDF(), diffuseColor(0.0f)
{
	reflectCoeff = 0.0f;
}

Lambertian::Lambertian(const Lambertian& lamb) : BRDF(lamb), diffuseColor(lamb.diffuseColor)
{
	reflectCoeff = lamb.reflectCoeff;
}

Lambertian::~Lambertian(void)
{
}

Lambertian* Lambertian::Clone(void) const 
{
	return new Lambertian(*this);
}	

Lambertian& Lambertian::operator=(const Lambertian& rhs) 
{
	if(this == &rhs) return *this;
		
	BRDF::operator=(rhs);
	
	reflectCoeff = rhs.reflectCoeff; 
	diffuseColor = rhs.diffuseColor;
	
	return *this;
}

RGBColor Lambertian::F(const ShadeRec& sr, const Vector3d& wo, const Vector3d& wi) const 
{
	return (reflectCoeff * diffuseColor * invPI);
}

RGBColor Lambertian::Rho(const ShadeRec& sr, const Vector3d& wo) const 
{
	return (reflectCoeff * diffuseColor);
}

void Lambertian::SetReflectCoeff(const float k) 
{
	reflectCoeff = k;
}

void Lambertian::SetDiffuseColor(const RGBColor& c) 
{
	diffuseColor = c;
}

void Lambertian::SetDiffuseColor(const float r, const float g, const float b) 
{
	diffuseColor.r = r; 
	diffuseColor.g = g; 
	diffuseColor.b = b;
}

void Lambertian::SetDiffuseColor(const float c) 
{
	diffuseColor.r = c;
	diffuseColor.g = c; 
	diffuseColor.b = c;
}
