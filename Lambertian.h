#pragma once
#include "BRDF.h"

class Lambertian : public BRDF
{
public:
	Lambertian(void);
	Lambertian(const Lambertian&);		
	~Lambertian(void);
		
	virtual Lambertian* Clone(void) const;
	Lambertian& operator= (const Lambertian& rhs);
		
	virtual RGBColor F(const ShadeRec&, const Vector3d&, const Vector3d&) const;
	virtual RGBColor Rho(const ShadeRec&, const Vector3d&) const;
							
	void SetReflectCoeff(const float);	
	void SetDiffuseColor(const RGBColor&);
	void SetDiffuseColor(const float, const float, const float);
	void SetDiffuseColor(const float);
					
private:
	float reflectCoeff;
	RGBColor diffuseColor;
};

