#pragma once
#include "Vector3d.h"
#include "RGBColor.h"
#include "Constants.h"
#include "Point3d.h"
#include "Point2d.h"
#include "ShadeRec.h"

class Light
{
public:
	Light(void);
	Light(const Light&);			
	~Light(void);

	Light& operator=(const Light&); 

	virtual Vector3d GetDirection(ShadeRec&) = 0;
	virtual RGBColor Radiance(ShadeRec&) = 0;
	virtual float GeometricFactor(ShadeRec& sr) {return 1.0f;}
	virtual float PDF(ShadeRec& sr) {return 1.0f;}

	void SetShadows(bool s) {shadows = s;}
	bool CastsShadow() {return shadows;}

	virtual bool InShadow(const Ray&, const ShadeRec&) const = 0;

protected:
	bool shadows;
};

