#pragma once
#include "RGBColor.h"
#include "Constants.h"
#include "ShadeRec.h"

class Material
{
public:
	Material(void);
	Material(const Material&);
	~Material(void);

	virtual Material* Clone(void) const = 0;	

	virtual RGBColor GetEmissiveRadiance(ShadeRec& sr);

	virtual RGBColor Shade(ShadeRec&);
	virtual RGBColor AreaLightShade(ShadeRec&);
	virtual RGBColor PathShade(ShadeRec&);

protected:
	Material& operator=(const Material&);
};

