#pragma once
#include <stdlib.h>
#include "Constants.h"
#include "RGBColor.h"

////
//Prototypes

double Min(double x0, double x1);
double Max(double x0, double x1);
float Clamp(float x, float min, float max);

////
//Inline Functions

inline double Min(double x0, double x1) 
{
	return (x0 < x1) ? x0 : x1;
}

inline double Max(double x0, double x1) 
{
	return (x0 > x1) ? x0 : x1;
}

inline float Clamp(float x, float min, float max)
{
	return x < min ? min : (x > max ? max : x);
}

