#pragma once

class Matrix
{
public:
	double	m[4][4];

public:
	Matrix(void);
	Matrix(const Matrix& mat);
	~Matrix(void);
			
	Matrix& operator= (const Matrix& rhs); 	
	Matrix operator* (const Matrix& mat) const;
	Matrix operator/ (const double d);

	void SetIdentity(void);	
};
