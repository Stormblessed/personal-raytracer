#include "Matte.h"

Matte::Matte(void) : Material(), ambientBRDF(new Lambertian()), diffuseBRDF(new Lambertian())
{
}

Matte::Matte(const Matte& m) : Material(m)
{
	if(m.ambientBRDF) ambientBRDF = m.ambientBRDF->Clone(); 
	else ambientBRDF = NULL;
	
	if(m.diffuseBRDF) diffuseBRDF = m.diffuseBRDF->Clone(); 
	else diffuseBRDF = NULL;
}

Material* Matte::Clone(void) const 
{
	return new Matte(*this);
}	

Matte& Matte::operator= (const Matte& rhs) 
{
	if(this == &rhs) return *this;
		
	Material::operator=(rhs);
	
	if(ambientBRDF) 
	{
		delete ambientBRDF;
		ambientBRDF = NULL;
	}
	if(rhs.ambientBRDF) ambientBRDF = rhs.ambientBRDF->Clone();
		
	if(diffuseBRDF) 
	{
		delete diffuseBRDF;
		diffuseBRDF = NULL;
	}
	if(rhs.diffuseBRDF) diffuseBRDF = rhs.diffuseBRDF->Clone();

	return *this;
}

Matte::~Matte(void) 
{
	if(ambientBRDF) 
	{
		delete ambientBRDF;
		ambientBRDF = NULL;
	}
	
	if(diffuseBRDF) 
	{
		delete diffuseBRDF;
		diffuseBRDF = NULL;
	}
}

RGBColor Matte::Shade(ShadeRec& sr) 
{
	Vector3d wo = -sr.ray.direction;
	RGBColor radiance = ambientBRDF->Rho(sr, wo) * sr.world.ambient_ptr->Radiance(sr);
	int numLights = sr.world.lights.size();
	
	for(int j = 0; j < numLights; j++) 
	{
		Vector3d wi = sr.world.lights[j]->GetDirection(sr);    
		float nDotWi = sr.normal * wi;
	
		if(nDotWi > 0.0)
		{
			bool inShadow = false;

			if(sr.world.lights[j]->CastsShadow())
			{
				Ray shadowRay(sr.hitPoint, wi);
				inShadow = sr.world.lights[j]->InShadow(shadowRay, sr);
			}
			if(!inShadow)
			{
				radiance += diffuseBRDF->F(sr, wo, wi) * sr.world.lights[j]->Radiance(sr) * nDotWi;
			}
		}
	}
	
	return radiance;
}

RGBColor Matte::AreaLightShade(ShadeRec& sr)
{
	Vector3d wo = -sr.ray.direction;
	RGBColor radiance = ambientBRDF->Rho(sr, wo) * sr.world.ambient_ptr->Radiance(sr);
	int numLights = sr.world.lights.size();
	
	for(int j = 0; j < numLights; j++) 
	{
		Vector3d wi = sr.world.lights[j]->GetDirection(sr);    
		float nDotWi = sr.normal * wi;
	
		if(nDotWi > 0.0)
		{
			bool inShadow = false;

			if(sr.world.lights[j]->CastsShadow())
			{
				Ray shadowRay(sr.hitPoint, wi);
				inShadow = sr.world.lights[j]->InShadow(shadowRay, sr);
			}
			if(!inShadow)
			{
				radiance += (diffuseBRDF->F(sr, wo, wi) * sr.world.lights[j]->Radiance(sr) * sr.world.lights[j]->GeometricFactor(sr) * nDotWi) / sr.world.lights[j]->PDF(sr);
			}
		}
	}
	
	return radiance;
}

void Matte::SetAmbientReflectCoeff(const float ka) 
{
	ambientBRDF->SetReflectCoeff(ka);
}

void Matte::SetDiffuseReflectCoeff(const float kd) 
{
	diffuseBRDF->SetReflectCoeff(kd);
}

void Matte::SetDiffuseColor(const RGBColor c) 
{
	ambientBRDF->SetDiffuseColor(c);
	diffuseBRDF->SetDiffuseColor(c);
}

void Matte::SetDiffuseColor(const float r, const float g, const float b)
{
	ambientBRDF->SetDiffuseColor(r, g, b);
	diffuseBRDF->SetDiffuseColor(r, g, b);
}

void Matte::SetDiffuseColor(const float c)
{
	ambientBRDF->SetDiffuseColor(c);
	diffuseBRDF->SetDiffuseColor(c);
}
