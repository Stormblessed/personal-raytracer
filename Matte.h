#pragma once
#include "Material.h"
#include "Lambertian.h"
#include "World.h"

class Matte : public Material
{
public:
	Matte(void);
	Matte(const Matte&);
	~Matte(void);

	virtual Material* Clone(void) const;	
	Matte& operator= (const Matte& rhs);	

	void SetAmbientReflectCoeff(const float);
	void SetDiffuseReflectCoeff(const float);
	void SetDiffuseColor(const RGBColor);		
	void SetDiffuseColor(const float r, const float g, const float b);
	void SetDiffuseColor(const float c);

	virtual RGBColor Shade(ShadeRec&);
	virtual RGBColor AreaLightShade(ShadeRec& sr);

private:
	Lambertian* ambientBRDF;
	Lambertian* diffuseBRDF;
};


