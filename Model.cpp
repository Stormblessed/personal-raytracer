#include "Model.h"

Model::Model(void)
{
	position = Point3d(0.f, 5.f, 0.f);
}

Model::~Model(void)
{
}

void Model::Setup(std::vector<ModelFace> faceSet, std::vector<Vector3d> vertexSet, std::vector<Vector3d> uvSet, std::vector<Vector3d> normalSet)
{
	faces = faceSet;
	vertices = vertexSet;
	uvs = uvSet;
	normals = normalSet;
}