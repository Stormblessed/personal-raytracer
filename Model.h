#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <iostream>
#include "ModelFace.h"
#include "Vector3d.h"
#include "Point3d.h"
#include "Material.h"

class Model
{
public:
	Model(void);
	~Model(void);

	void Setup(std::vector<ModelFace>, std::vector<Vector3d>, std::vector<Vector3d>, std::vector<Vector3d>);

public:
	std::vector<ModelFace> faces;
	std::vector<Vector3d> vertices;
	std::vector<Vector3d> uvs; //Should be Vector2d
	std::vector<Vector3d> normals;

	Point3d position;

	Material* material_ptr;
};

