#pragma once
#include <vector>

class ModelFace
{
public:
	ModelFace(void);
	~ModelFace(void);

	std::vector<int> vertexIndices;
	std::vector<int> uvIndices;
	std::vector<int> normalIndices;
};

