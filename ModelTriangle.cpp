#include "ModelTriangle.h"

ModelTriangle::ModelTriangle(void) : GeometricObject(), model_ptr(NULL), index0(0), index1(0), index2(0), normal()
{
}

ModelTriangle::ModelTriangle(const ModelTriangle& mt) : GeometricObject(mt), model_ptr(mt.model_ptr), index0(mt.index0), index1(mt.index1), index2(mt.index2), normal(mt.normal)
{
}

ModelTriangle::~ModelTriangle(void) 
{
	if(model_ptr) 
	{
		delete model_ptr;
		model_ptr = NULL;
	}
}

ModelTriangle* ModelTriangle::Clone(void) const
{
	return new ModelTriangle(*this);
}

ModelTriangle& ModelTriangle::operator=(const ModelTriangle& rhs) 
{
	if(this == &rhs) return *this;

	GeometricObject::operator= (rhs);
	
	model_ptr = rhs.model_ptr;
	index0 = rhs.index0;
	index1 = rhs.index1;
	index2 = rhs.index2;
	normal = rhs.normal;
	
	return *this;
}

void ModelTriangle::ComputeNormal(const bool reverseNormal) 
{
	normal = (model_ptr->vertices[index1] - model_ptr->vertices[index0]) ^ (model_ptr->vertices[index2] - model_ptr->vertices[index0]);
	normal.Normalize();
	
	if(reverseNormal) normal = -normal;
}

Normal ModelTriangle::GetNormal(void) const 
{
	return normal;
}	

BBox ModelTriangle::GetBoundingBox(void) 
{
	double delta = 0.000001;

	Point3d v0(model_ptr->vertices[index0]);
	Point3d v1(model_ptr->vertices[index1]);
	Point3d v2(model_ptr->vertices[index2]);
	
	double x0 = Min(Min(v0.x, v1.x), v2.x) - delta;
	double x1 = Max(Max(v0.x, v1.x), v2.x) + delta;
	
	double y0 = Min(Min(v0.y, v1.y), v2.y) - delta;
	double y1 = Max(Max(v0.y, v1.y), v2.y) + delta;
	//std::cout << v0.y << " : " << v1.y << " : " << v2.y << std::endl;
	//std::cout << y0 << ", " << y1 << std::endl;

	double z0 = Min(Min(v0.z, v1.z), v2.z) - delta;
	double z1 = Max(Max(v0.z, v1.z), v2.z) + delta;

	return BBox(x0, x1, y0, y1, z0, z1);
}

bool ModelTriangle::Hit(const Ray& ray, double& tMin, ShadeRec& sr) const 
{	
	Point3d v0(model_ptr->vertices[index0]);
	Point3d v1(model_ptr->vertices[index1]);
	Point3d v2(model_ptr->vertices[index2]);

	double a = v0.x - v1.x;
	double b = v0.x - v2.x;
	double c = ray.direction.x;
	double d = v0.x - ray.origin.x; 

	double e = v0.y - v1.y;
	double f = v0.y - v2.y;
	double g = ray.direction.y;
	double h = v0.y - ray.origin.y;

	double i = v0.z - v1.z;
	double j = v0.z - v2.z;
	double k = ray.direction.z;
	double l = v0.z - ray.origin.z;

		
	double m = f * k - g * j;
	double n = h * k - g * l;
	double p = f * l - h * j;
	double q = g * i - e * k;
	double s = e * j - f * i;
	
	double invDenom  = 1.0 / (a * m + b * q + c * s);
	
	double e1 = d * m - b * n - c * p;
	double beta = e1 * invDenom;
	
	if(beta < 0.0) return false;
	
	double r = r = e * l - h * i;
	double e2 = a * n + d * q + c * r;
	double gamma = e2 * invDenom;
	
	if(gamma < 0.0 ) return false;
	
	if(beta + gamma > 1.0) return false;
			
	double e3 = a * p - b * r + d * s;
	double t = e3 * invDenom;
	
	if(t < kEpsilon) return false;
					
	tMin = t;
	sr.normal = normal;  	
	sr.localHitPoint = ray.origin + t * ray.direction;	
	
	return true;	
}  		

bool ModelTriangle::ShadowHit(const Ray& ray, float& tMin) const 
{	
	Point3d v0(model_ptr->vertices[index0]);
	Point3d v1(model_ptr->vertices[index1]);
	Point3d v2(model_ptr->vertices[index2]);

	double a = v0.x - v1.x;
	double b = v0.x - v2.x;
	double c = ray.direction.x;
	double d = v0.x - ray.origin.x; 

	double e = v0.y - v1.y;
	double f = v0.y - v2.y;
	double g = ray.direction.y;
	double h = v0.y - ray.origin.y;

	double i = v0.z - v1.z;
	double j = v0.z - v2.z;
	double k = ray.direction.z;
	double l = v0.z - ray.origin.z;
		

	double m = f * k - g * j;
	double n = h * k - g * l;
	double p = f * l - h * j;
	double q = g * i - e * k;
	double s = e * j - f * i;
	
	double invDenom  = 1.0 / (a * m + b * q + c * s);
	
	double e1 = d * m - b * n - c * p;
	double beta = e1 * invDenom;
	
	if(beta < 0.0) return false;
	
	double r = r = e * l - h * i;
	double e2 = a * n + d * q + c * r;
	double gamma = e2 * invDenom;
	
	if(gamma < 0.0 ) return false;
	
	if(beta + gamma > 1.0) return false;
			
	double e3 = a * p - b * r + d * s;
	double t = e3 * invDenom;
	
	if(t < kEpsilon) return false;
					
	tMin = t;
	
	return true;	
}  
