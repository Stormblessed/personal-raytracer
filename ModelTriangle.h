#pragma once
#include "GeometricObject.h"
#include "Model.h"
#include "MathHelper.h"

class ModelTriangle : public GeometricObject
{
public:
	Model* model_ptr;
	int index0, index1, index2;
	Normal normal;
	float area;

public:
	ModelTriangle(void);
	ModelTriangle(const ModelTriangle& mt);
	~ModelTriangle(void);

	virtual ModelTriangle* Clone(void) const;
	ModelTriangle& operator=(const ModelTriangle& rhs);

	virtual bool Hit(const Ray& ray, double& tMin, ShadeRec& sr) const;		 					 
	virtual bool ShadowHit(const Ray& ray, float& tMin) const; 

	void ComputeNormal(const bool reverseNormal);
	virtual Normal GetNormal(void) const;
	virtual BBox GetBoundingBox(void);

};

