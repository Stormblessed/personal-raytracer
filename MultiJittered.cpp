#include "MultiJittered.h"

MultiJittered::MultiJittered(void) : Sampler()
{
}

MultiJittered::MultiJittered(const int ns) : Sampler(ns) 
{
	GenerateSamples();
}

MultiJittered::MultiJittered(const int ns, const int m) : Sampler(ns, m) 
{
	GenerateSamples();
}

MultiJittered::MultiJittered(const MultiJittered& js) : Sampler(js) 
{
	GenerateSamples();
}

MultiJittered::~MultiJittered(void)
{
}

MultiJittered& MultiJittered::operator=(const MultiJittered& rhs) 
{
	if(this == &rhs) return *this;
		
	Sampler::operator= (rhs);

	return *this;
}

MultiJittered* MultiJittered::Clone(void) const 
{
	return new MultiJittered(*this);
}


void MultiJittered::GenerateSamples(void) 
{		
	int n = (int)sqrt((float)numSamples);
	float subcellWidth = 1.0 / ((float) numSamples);
		
	Point2d fillPoint;
	for (int j = 0; j < numSamples * numSets; j++)
		samples.push_back(fillPoint);
		
	// distribute points in the initial patterns
	for(int p = 0; p < numSets; p++)
	{
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++) 
			{
				samples[i * n + j + p * numSamples].x = (i * n + j) * subcellWidth + RandFloat(0, subcellWidth);
				samples[i * n + j + p * numSamples].y = (j * n + i) * subcellWidth + RandFloat(0, subcellWidth);
			}
		}
	}
	
	// shuffle x coordinates
	for(int p = 0; p < numSets; p++)
	{
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++) 
			{
				int k = RandInt(j, n - 1);
				float t = samples[i * n + j + p * numSamples].x;
				samples[i * n + j + p * numSamples].x = samples[i * n + k + p * numSamples].x;
				samples[i * n + k + p * numSamples].x = t;
			}
		}
	}

	// shuffle y coordinates
	for(int p = 0; p < numSets; p++)
	{
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < n; j++) 
			{
				int k = RandInt(j, n - 1);
				float t = samples[j * n + i + p * numSamples].y;
				samples[j * n + i + p * numSamples].y = samples[k * n + i + p * numSamples].y;
				samples[k * n + i + p * numSamples].y = t;
			}
		}
	}
}
