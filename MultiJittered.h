#pragma once
#include "Sampler.h"

class MultiJittered : public Sampler
{
public:
	MultiJittered(void);
	MultiJittered(const int);					
	MultiJittered(const int, const int);	
	MultiJittered(const MultiJittered&);	
	virtual ~MultiJittered(void);

	MultiJittered& operator=(const MultiJittered&);

	virtual MultiJittered* Clone(void) const;

private:
	virtual void GenerateSamples(void);
};

