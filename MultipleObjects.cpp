#include "MultipleObjects.h"
#include <iostream>

MultipleObjects::MultipleObjects(void)
{
}

MultipleObjects::MultipleObjects(World* _worldPtr) : Tracer(_worldPtr)
{
}

MultipleObjects::~MultipleObjects(void)
{
}

RGBColor MultipleObjects::TraceRay(const Ray& ray) const 
{
	ShadeRec sr(world_ptr->HitBareBonesObjects(ray));

	if(sr.hitAnObject) return sr.color;  
	else return world_ptr->backgroundColor;   
}

RGBColor MultipleObjects::TraceRay(const Ray ray, const int depth) const
{
	ShadeRec sr(world_ptr->HitBareBonesObjects(ray));

	if(sr.hitAnObject) return sr.color;  
	else return world_ptr->backgroundColor;
}
