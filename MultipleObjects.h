#pragma once
#include "Tracer.h"
#include "World.h"

class MultipleObjects : public Tracer
{
public:
	MultipleObjects(void);
	MultipleObjects(World*);
	virtual ~MultipleObjects(void);

	virtual RGBColor TraceRay(const Ray& ray) const;
	virtual RGBColor TraceRay(const Ray ray, const int depth) const;
};

