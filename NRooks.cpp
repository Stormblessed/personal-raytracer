#include "NRooks.h"

NRooks::NRooks(void) : Sampler()
{
}

NRooks::NRooks(const int ns) : Sampler(ns) 
{
	GenerateSamples();
}

NRooks::NRooks(const int ns, const int m) : Sampler(ns, m) 
{
	GenerateSamples();
}

NRooks::NRooks(const NRooks& js) : Sampler(js) 
{
	GenerateSamples();
}

NRooks::~NRooks(void)
{
}

NRooks& NRooks::operator=(const NRooks& rhs) 
{
	if(this == &rhs) return *this;
		
	Sampler::operator= (rhs);

	return *this;
}

NRooks* NRooks::Clone(void) const 
{
	return new NRooks(*this);
}

void NRooks::GenerateSamples(void) 
{	
	for(int p = 0; p < numSets; p++)  
	{
		for (int j = 0; j < numSamples; j++) 
		{
			Point2d samplePoint((j + RandFloat()) / numSamples, (j + RandFloat()) / numSamples);
			samples.push_back(samplePoint);
		}
	}

	ShuffleXCoordinates();
	ShuffleYCoordinates();
}
