#pragma once
#include "Sampler.h"

class NRooks : public Sampler
{
public:
	NRooks(void);
	NRooks(const int);					
	NRooks(const int, const int);	
	NRooks(const NRooks&);	
	virtual ~NRooks(void);

	NRooks& operator=(const NRooks&);

	virtual NRooks* Clone(void) const;

private:
	virtual void GenerateSamples(void);
};

