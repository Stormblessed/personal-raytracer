#pragma once
#include "Matrix.h"
#include "Vector3d.h"
#include "Point3d.h"

class Normal 
{	
	public:
		double	x, y, z;
				
	public:
		Normal(void);
		Normal(double a);
		Normal(double _x, double _y, double _z);
		Normal(const Normal& n); 
		Normal(const Vector3d& v);
		~Normal(void);

		Normal& operator=(const Normal& rhs); 
		Normal& operator=(const Vector3d& rhs);
		Normal& operator=(const Point3d& rhs);
		Normal 	operator-(void) const;	
		Normal operator+(const Normal& n) const;
		Normal& operator+=(const Normal& n);
		double operator*(const Vector3d& v) const;				
		Normal operator*(const double a) const;	
		
		void Normalize(void); 									 		
};

// inlined non-member functions

Normal operator* (const double a, const Normal& n);
inline Normal operator*(const double f, const Normal& n) 
{
	return (Normal(f * n.x, f * n.y,f * n.z));
}

Vector3d operator+ (const Vector3d& v, const Normal& n);
inline Vector3d operator+(const Vector3d& v, const Normal& n)
{	
	return (Vector3d(v.x + n.x, v.y + n.y, v.z + n.z));
}	

Vector3d operator- (const Vector3d&, const Normal& n);
inline Vector3d operator-(const Vector3d& v, const Normal& n) 
{
	return (Vector3d(v.x - n.x, v.y - n.y, v.z - n.z));
}

double operator* (const Vector3d& v, const Normal& n);
inline double operator*(const Vector3d& v, const Normal& n) 
{
	return (v.x * n.x + v.y * n.y + v.z * n.z);     
}

// non-inlined non-member function

Normal operator* (const Matrix& mat, const Normal& n);