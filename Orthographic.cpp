#include "Orthographic.h"

Orthographic::Orthographic(void) : Camera()
{
}

Orthographic::Orthographic(const Orthographic& cam) : Camera(cam)	
{
}

Orthographic::~Orthographic(void)
{
}

Orthographic& Orthographic::operator= (const Orthographic& rhs) 
{ 	
	if(this == &rhs) return *this;
		
	Camera::operator=(rhs);

	return *this;
}

Vector3d Orthographic::Forward() const
{
	Vector3d dir = (lookAt - eye);
	dir.Normalize();
	return dir;
}

void Orthographic::Render(World& w)
{
	w.image.create(w.vp.hRes, w.vp.vRes, sf::Color::White);

	RGBColor radiance;
	ViewPlane vp(w.vp);
	Ray ray;
	int depth = 0;

	Point2d sampledPoint;
	Point2d pixelPoint;

	ray.direction = Forward();

	for(int r = 0; r < vp.vRes; r++)
	{
		for(int c = 0; c < vp.hRes; c++)
		{
			radiance = black;

			for(int j = 0; j < vp.numSamples; j++)
			{
				sampledPoint = vp.sampler_ptr->SampleUnitSquare();
				pixelPoint.x = vp.pixelSize * (c - 0.5f * vp.hRes + sampledPoint.x);
				pixelPoint.y = vp.pixelSize * (r - 0.5f * vp.vRes + sampledPoint.y);

				ray.origin = Point3d(pixelPoint.x, pixelPoint.y, eye.z);
				radiance += w.tracer_ptr->TraceRay(ray);
			}

			radiance /= vp.numSamples;
			sf::Color resultColor = sf::Color(radiance.r * 255, radiance.g * 255, radiance.b * 255, 255);

			int screenX = c;
			int screenY = vp.vRes - r - 1;
			w.image.setPixel(screenX, screenY, resultColor);
		}
	}
}
