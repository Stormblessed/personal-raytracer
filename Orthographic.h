#pragma once
#include "Camera.h"
#include "World.h"

class Orthographic : public Camera
{
public:
	Orthographic(void);
	Orthographic(const Orthographic&);	
	virtual ~Orthographic(void);

	Orthographic& operator= (const Orthographic&);	

	Vector3d Forward() const;
	virtual void Render(World&);
};

