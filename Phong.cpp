#include "Phong.h"

Phong::Phong(void) : Material(), ambientBRDF(new Lambertian()), diffuseBRDF(new Lambertian()), specularBRDF(new GlossySpecular())
{
}

Phong::Phong(const Phong& m) : Material(m)
{
	if(m.ambientBRDF) ambientBRDF = m.ambientBRDF->Clone(); 
	else ambientBRDF = NULL;
	
	if(m.diffuseBRDF) diffuseBRDF = m.diffuseBRDF->Clone(); 
	else diffuseBRDF = NULL;

	if(m.specularBRDF) specularBRDF = m.specularBRDF->Clone();
	else specularBRDF = NULL;
}

Material* Phong::Clone(void) const 
{
	return new Phong(*this);
}	

Phong& Phong::operator= (const Phong& rhs) 
{
	if(this == &rhs) return *this;
		
	Material::operator=(rhs);
	
	if(ambientBRDF) 
	{
		delete ambientBRDF;
		ambientBRDF = NULL;
	}
	if(rhs.ambientBRDF) ambientBRDF = rhs.ambientBRDF->Clone();
		
	if(diffuseBRDF) 
	{
		delete diffuseBRDF;
		diffuseBRDF = NULL;
	}
	if(rhs.diffuseBRDF) diffuseBRDF = rhs.diffuseBRDF->Clone();

	if(specularBRDF)
	{
		delete specularBRDF;
		specularBRDF = NULL;
	}
	if(rhs.specularBRDF) specularBRDF = rhs.specularBRDF->Clone();

	return *this;
}

Phong::~Phong(void) 
{
	if(ambientBRDF) 
	{
		delete ambientBRDF;
		ambientBRDF = NULL;
	}
	
	if(diffuseBRDF) 
	{
		delete diffuseBRDF;
		diffuseBRDF = NULL;
	}

	if(specularBRDF) 
	{
		delete specularBRDF;
		specularBRDF = NULL;
	}
}

RGBColor Phong::Shade(ShadeRec& sr) 
{
	Vector3d wo = -sr.ray.direction;
	RGBColor radiance = ambientBRDF->Rho(sr, wo) * sr.world.ambient_ptr->Radiance(sr);
	int numLights = sr.world.lights.size();
	
	for(int j = 0; j < numLights; j++) 
	{
		Vector3d wi = sr.world.lights[j]->GetDirection(sr);    
		float nDotWi = sr.normal * wi;
	
		if(nDotWi > 0.0)
		{
			bool inShadow = false;
			if(sr.world.lights[j]->CastsShadow())
			{
				Ray shadowRay(sr.hitPoint, wi);
				inShadow = sr.world.lights[j]->InShadow(shadowRay, sr);
			}
			if(!inShadow)
			{
				radiance += diffuseBRDF->F(sr, wo, wi) + specularBRDF->F(sr, wo, wi) * sr.world.lights[j]->Radiance(sr) * nDotWi;
			}
		}
	}
	
	return radiance;
}

void Phong::SetAmbientReflectCoeff(const float ka) 
{
	ambientBRDF->SetReflectCoeff(ka);
}

void Phong::SetDiffuseReflectCoeff(const float kd) 
{
	diffuseBRDF->SetReflectCoeff(kd);
}

void Phong::SetSpecularReflectCoeff(const float kd)
{
	specularBRDF->SetReflectCoeff(kd);
}

void Phong::SetDiffuseColor(const RGBColor c) 
{
	ambientBRDF->SetDiffuseColor(c);
	diffuseBRDF->SetDiffuseColor(c);
	specularBRDF->SetSpecularColor(c);
}

void Phong::SetDiffuseColor(const float r, const float g, const float b)
{
	ambientBRDF->SetDiffuseColor(r, g, b);
	diffuseBRDF->SetDiffuseColor(r, g, b);
	specularBRDF->SetSpecularColor(r, g, b);
}

void Phong::SetDiffuseColor(const float c)
{
	ambientBRDF->SetDiffuseColor(c);
	diffuseBRDF->SetDiffuseColor(c);
	specularBRDF->SetSpecularColor(c);
}

void Phong::SetSpecularExponent(const float f)
{
	specularBRDF->SetExponent(f);
}