#pragma once
#include "Material.h"
#include "Lambertian.h"
#include "GlossySpecular.h"
#include "World.h"

class Phong : public Material
{
public:
	Phong(void);
	Phong(const Phong&);
	~Phong(void);

	virtual Material* Clone(void) const;	
	Phong& operator= (const Phong& rhs);

	virtual RGBColor Shade(ShadeRec&);

	void SetAmbientReflectCoeff(const float);
	void SetDiffuseReflectCoeff(const float);
	void SetSpecularReflectCoeff(const float);
	void SetDiffuseColor(const RGBColor);		
	void SetDiffuseColor(const float r, const float g, const float b);
	void SetDiffuseColor(const float c);
	void SetSpecularExponent(const float f);

protected:
	Lambertian* ambientBRDF;
	Lambertian* diffuseBRDF;
	GlossySpecular* specularBRDF;
};

