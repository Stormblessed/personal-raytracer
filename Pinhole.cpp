#include "Pinhole.h"
#include <math.h>
#include <iostream>
#include <stdlib.h>

Pinhole::Pinhole(void) : Camera()
{
	distance = 500.0f;
	zoom = 1.0f;
}

Pinhole::Pinhole(const Pinhole& cam) : Camera(cam)	
{
	distance = cam.distance;
	zoom = cam.zoom;
}

Pinhole::~Pinhole(void)
{
}

Pinhole& Pinhole::operator= (const Pinhole& rhs) 
{ 	
	if(this == &rhs) return *this;
		
	Camera::operator= (rhs);

	distance = rhs.distance;
	zoom = rhs.zoom;

	return *this;
}

Vector3d Pinhole::RayDirection(const Point2d& p) const
{
	Vector3d dir = p.x * u + p.y * v - distance * w;
	dir.Normalize();
	return dir;
}

void Pinhole::Render(World& w)
{
	w.image.create(w.vp.hRes, w.vp.vRes, sf::Color::White);

	RGBColor radiance;
	ViewPlane vp(w.vp);
	Ray ray;
	int depth = 0;

	Point2d sampledPoint;
	Point2d pixelPoint;

	vp.pixelSize /= zoom;
	ray.origin = eye;

	float totalPixels = vp.vRes * vp.hRes;

	for(int r = 0; r < vp.vRes; r++)
	{
		for(int c = 0; c < vp.hRes; c++)
		{
			radiance = black;

			for(int j = 0; j < vp.numSamples; j++)
			{
				sampledPoint = vp.sampler_ptr->SampleUnitSquare();
				pixelPoint.x = vp.pixelSize * (c - 0.5f * vp.hRes + sampledPoint.x);
				pixelPoint.y = vp.pixelSize * (r - 0.5f * vp.vRes + sampledPoint.y);
				
				ray.direction = RayDirection(pixelPoint);
				radiance += w.tracer_ptr->TraceRay(ray, depth);
			}

			radiance /= vp.numSamples;
			radiance *= exposureTime;
			sf::Color resultColor = sf::Color(radiance.r * 255, radiance.g * 255, radiance.b * 255, 255);

			int screenX = c;
			int screenY = vp.vRes - r - 1;

			w.image.setPixel(screenX, screenY, resultColor);
		}
		
		system("CLS");
		std::cout << "Percent Rendered: " << (float)((r * vp.vRes) / totalPixels) * 100.0f << "%" << std::endl;
	}

	system("CLS");
	std::cout << "Percent Rendered: 100%" << std::endl << "RENDER COMPLETE!" << std::endl;
}

void Pinhole::SetDistance(float _d) 
{
	distance = _d;
}	

void Pinhole::SetZoom(float zoomFactor) {
	zoom = zoomFactor;
}	
