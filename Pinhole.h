#pragma once
#include "Camera.h"
#include "Point2d.h"
#include "World.h"

class Pinhole : public Camera
{
public:
	Pinhole(void);
	Pinhole(const Pinhole&);	
	virtual ~Pinhole(void);

	Pinhole& operator= (const Pinhole&);	

	Vector3d RayDirection(const Point2d&) const;
	virtual void Render(World&);

	void SetDistance(const float);
	void SetZoom(const float);

private:
	float distance;
	float zoom;
};

