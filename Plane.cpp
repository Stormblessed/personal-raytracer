#include "Plane.h"

const double Plane::kEpsilon = 0.001;

Plane::Plane(void)
{
}

Plane::Plane(const Point3d p, const Normal& n)
{
	point = p;
	normal = n;
}

Plane::~Plane(void)
{
}

Plane* Plane::Clone(void) const 
{
	return new Plane(this->point, this->normal);
}

bool Plane::Hit(const Ray& ray, double& tMin, ShadeRec& sr) const
{
	double t = (point - ray.origin) * normal / (ray.direction * normal);

	if(t > kEpsilon)
	{
		tMin = t;
		sr.normal = normal;
		sr.localHitPoint = ray.origin + t * ray.direction;

		return true;
	}
	else return false;

}

bool Plane::ShadowHit(const Ray& ray, float& tMin) const
{
	float t = (point - ray.origin) * normal / (ray.direction * normal);
	if(t > kEpsilon)
	{
		tMin = t;
		return true;
	}
	else return false;
}
