#pragma once
#include "GeometricObject.h"
#include "Point3d.h"
#include "Normal.h"

class Plane : public GeometricObject
{
public:
	Plane(void);
	Plane(const Point3d, const Normal&);
	~Plane(void);

	virtual Plane* Clone(void) const;

	virtual bool Hit(const Ray& ray, double& tMin, ShadeRec& sr) const;
	virtual bool ShadowHit(const Ray& ray, float& tMin) const;

private:
	Point3d point;
	Normal normal;
	static const double kEpsilon;
};

