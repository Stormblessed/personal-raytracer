#include "Point2d.h"

Point2d::Point2d(void)
{
	x = 0.0f;
	y = 0.0f;
}

Point2d::Point2d(const float arg)
{
	x = arg;
	y = arg;
}

Point2d::Point2d(const float _x, const float _y)
{
	x = _x;
	y = _y;
}

Point2d::Point2d(const Point2d& p)
{
	x = p.x;
	y = p.y;
}

Point2d::~Point2d(void)
{
}

Point2d& Point2d::operator=(const Point2d& rhs) 
{
	if(this == &rhs) return *this;

	x = rhs.x;
	y = rhs.y;

	return *this;
}

Point2d Point2d::operator*(const float a) 
{
	return Point2d(a * x, a * y);
}
