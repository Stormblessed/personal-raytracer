#pragma once

class Point2d
{
public:
	float x, y;

public:
	Point2d(void);
	Point2d(const float);
	Point2d(const float, const float);
	Point2d(const Point2d&);
	~Point2d(void);

	Point2d& operator=(const Point2d&);
	Point2d	operator*(const float);
};

