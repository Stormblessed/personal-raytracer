#include <math.h>
#include "Point3D.h"

Point3d::Point3d(void)
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
}

Point3d::Point3d(const double a)
{
	x = a;
	y = a;
	z = a;
}

Point3d::Point3d(const double a, const double b, const double c)
{
	x = a;
	y = b;
	z = c;
}

Point3d::Point3d(const Point3d& p)
{
	x = p.x;
	y = p.y;
	z = p.z;
}

Point3d::Point3d(const Vector3d& v)
{
	x = v.x;
	y = v.y;
	z = v.z;
}

Point3d::~Point3d() 
{
}

Point3d& Point3d::operator=(const Point3d& rhs) 
{
	
	if(this == &rhs)
		return (*this);

	x = rhs.x; y = rhs.y; z = rhs.z;

	return (*this);
}

double Point3d::Distance(const Point3d& p) const 
{
	return (sqrt((x - p.x) * (x - p.x) 
			   + (y - p.y) * (y - p.y)
			   + (z - p.z) * (z - p.z)));
}

Point3d Point3d::operator-(void) const 
{
	return (Point3d(-x, -y, -z));
}

Vector3d Point3d::operator-(const Point3d& p) const 
{
	return (Vector3d(x - p.x,y - p.y,z - p.z));
}

Point3d Point3d::operator+(const Vector3d& v) const 
{
	return (Point3d(x + v.x, y + v.y, z + v.z));
}

Point3d Point3d::operator-(const Vector3d& v) const 
{
	return (Point3d(x - v.x, y - v.y, z - v.z));
}

Point3d Point3d::operator*(const double a) const 
{
	return (Point3d(x * a,y * a,z * a));
}

double Point3d::DistanceSquared(const Point3d& p) const 
{
	return ((x - p.x) * (x - p.x) 
		  + (y - p.y) * (y - p.y)
		  +	(z - p.z) * (z - p.z));
}

// non-member function

Point3d operator* (const Matrix& mat, const Point3d& p) 
{
	return (Point3d(mat.m[0][0] * p.x + mat.m[0][1] * p.y + mat.m[0][2] * p.z + mat.m[0][3],
					mat.m[1][0] * p.x + mat.m[1][1] * p.y + mat.m[1][2] * p.z + mat.m[1][3],
					mat.m[2][0] * p.x + mat.m[2][1] * p.y + mat.m[2][2] * p.z + mat.m[2][3]));
}

