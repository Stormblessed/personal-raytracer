#pragma once
#include "Matrix.h"
#include "Vector3d.h"

class Point3d
{
public:
	double x, y, z;

public:
	Point3d();													
	Point3d(const double a);									
	Point3d(const double a, const double b, const double c);
	Point3d(const Point3d& p);
	Point3d(const Vector3d& v);
	~Point3d();	
		
	Point3d& operator=(const Point3d& p);
	Point3d operator-(void) const;
	Vector3d operator-(const Point3d& p) const;
	Point3d operator+(const Vector3d& v) const;
	Point3d operator-(const Vector3d& v) const;		
	Point3d operator*(const double a) const;
		
	double DistanceSquared(const Point3d& p) const;
	double Distance(const Point3d& p) const;
};

// inlined non-member function

Point3d operator* (double a, const Point3d& p);
inline Point3d operator*(double a, const Point3d& p) 
{
	return (Point3d(a * p.x, a * p.y, a * p.z));
}

// non-inlined non-member function

Point3d operator* (const Matrix& mat, const Point3d& p);


