#include "PointLight.h"
#include "World.h"

PointLight::PointLight(void) : Light(), color(1.0f), location(0.0f)
{
	intensity = 1.0f;
}

PointLight::PointLight(const PointLight& a) : Light(a), color(a.color), location(a.location)
{
	intensity = a.intensity;
}

PointLight::~PointLight(void)
{
}

PointLight& PointLight::operator=(const PointLight& rhs) 
{
	if(this == &rhs) return *this;

	Light::operator=(rhs);
	
	intensity = rhs.intensity; 
	color = rhs.color;
	location = rhs.location;

	return *this;
}

Vector3d PointLight::GetDirection(ShadeRec& sr)
{
	return (location - sr.hitPoint).Hat();
}

RGBColor PointLight::Radiance(ShadeRec& sr)
{
	return intensity * color;
}

bool PointLight::InShadow(const Ray& ray, const ShadeRec& sr) const
{
	float t;
	int numObjects = sr.world.objects.size();
	float distance = (location - ray.origin).Length();

	for(int i = 0; i < numObjects; i++)
	{
		if(sr.world.objects[i]->ShadowHit(ray, t) && t < distance) return true;
	}
	return false;
}


