#pragma once
#include "Light.h"

class PointLight : public Light
{
public:
	PointLight(void);
	PointLight(const PointLight&);
	~PointLight(void);

	PointLight& operator=(const PointLight&);

	virtual Vector3d GetDirection(ShadeRec&);
	virtual RGBColor Radiance(ShadeRec&);

	void SetIntensity(float i) {intensity = i;}
	void SetColor(RGBColor c) {color = c;}
	void SetColor(float r, float g, float b) {color = RGBColor(r, g, b);}
	void SetColor(float c) {color = RGBColor(c);}
	void SetLocation(Vector3d l) {location = l;}

	virtual bool InShadow(const Ray&, const ShadeRec&) const;

private:
	float intensity;
	RGBColor color;
	Vector3d location;
};

