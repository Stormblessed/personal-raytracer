#include "PureRandom.h"

PureRandom::PureRandom(void) : Sampler()
{
}

PureRandom::PureRandom(const int num) : Sampler(num) 
{
	GenerateSamples();
}

PureRandom::PureRandom(const PureRandom& r)	: Sampler(r) 
{
	GenerateSamples();
}

PureRandom::~PureRandom(void)
{
}

PureRandom& PureRandom::operator=(const PureRandom& rhs) 
{
	if(this == &rhs) return *this;
		
	Sampler::operator=(rhs);

	return *this;
}

PureRandom*	PureRandom::Clone(void) const 
{
	return (new PureRandom(*this));
}

void PureRandom::GenerateSamples(void) 
{
	for(int p = 0; p < numSets; p++)   
	{
		for(int q = 0; q < numSamples; q++)
		{
			samples.push_back(Point2d(RandFloat(), RandFloat()));
		}
	}
}
