#pragma once
#include "Sampler.h"

class PureRandom : public Sampler
{
public:
	PureRandom(void);
	PureRandom(const int);					
	PureRandom(const PureRandom&);	
	virtual ~PureRandom(void);

	PureRandom& operator=(const PureRandom& rhs);

	virtual PureRandom*	Clone(void) const;	

private:
	virtual void GenerateSamples(void);
};


