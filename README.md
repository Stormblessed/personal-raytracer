# Personal Raytracer C++ Portfolio Code

## This is the source for a raytracer I developed as a learning exercise last year.  It supports BRDF-based materials, grid-accelerated triangle meshes, and multiple camera types.  For testing purposes I also wrote a small python plugin, not present in the source here, that allows a build of this renderer to be used as the primary renderer in Blender.