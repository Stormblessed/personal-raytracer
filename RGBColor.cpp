#include "RGBColor.h"

RGBColor::RGBColor(void)
{
	r = 0.0f;
	g = 0.0f;
	b = 0.0f;
}

RGBColor::RGBColor(float all)
{
	r = all;
	g = all;
	b = all;
}

RGBColor::RGBColor(float _r, float _g, float _b)
{
	r = _r;
	g = _g;
	b = _b;
}

RGBColor::RGBColor(const RGBColor& c)
{
	r = c.r;
	g = c.g;
	b = c.b;
}

RGBColor::~RGBColor(void)
{
}

RGBColor RGBColor::operator+ (const RGBColor& c) const
{
	return MaxToOne(RGBColor(r + c.r, g + c.g, b + c.b));
}

RGBColor& RGBColor::operator+=(const RGBColor& c) 
{
	r += c.r; g += c.g; b += c.b;
    return MaxToOne(*this);
}

RGBColor RGBColor::operator*(const float a) const 
{
	return MaxToOne(RGBColor (r * a, g * a, b * a));	
}

RGBColor& RGBColor::operator*=(const float a) 
{
	r *= a; g *= a; b *= a;
	return MaxToOne(*this);
}

RGBColor RGBColor::operator/(const float a) const 
{
	return MaxToOne(RGBColor (r / a, g / a, b / a));
}

RGBColor& RGBColor::operator/=(const float a) 
{	
	r /= a;
	g /= a;
	b /= a;
	return MaxToOne(*this);
}

RGBColor RGBColor::operator*(const RGBColor& c) const 
{
	return MaxToOne(RGBColor (r * c.r, g * c.g, b * c.b));
} 

bool RGBColor::operator==(const RGBColor& c) const 
{
	return (r == c.r && g == c.g && b == c.b);
}

RGBColor& RGBColor::operator= (const RGBColor& rhs) 
{
	if(this == &rhs) return *this;

	r = rhs.r;
	g = rhs.g;
	b = rhs.b;

	return *this;
}
 
RGBColor RGBColor::PowC(float p) const 
{
	return MaxToOne(RGBColor(pow(r, p), pow(g, p), pow(b, p)));
}

RGBColor RGBColor::MaxToOne(RGBColor& c) const
{
	RGBColor color = c;
	if(c.r > 1.0)
		color.r = 1.0f;
	if(c.g > 1.0)
		color.g = 1.0f;
	if(c.b > 1.0)
		color.b = 1.0f;
	return color;
}

float RGBColor::Average(void) const 
{
	return (0.333333333333 * (r + g + b));
}
