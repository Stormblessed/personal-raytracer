#pragma once
#include <math.h>
#include <string>

class RGBColor
{
public:
	float r;
	float g;
	float b;

public:
	RGBColor(void);
	RGBColor(float);
	RGBColor(float, float, float);
	RGBColor(const RGBColor&);
	~RGBColor(void);

	RGBColor& operator=(const RGBColor&); 
	RGBColor operator+(const RGBColor&) const;	
	RGBColor& operator+=(const RGBColor&);
	RGBColor operator*(const float) const;
	RGBColor& operator*=(const float);					
	RGBColor operator/(const float) const;
	RGBColor& operator/=(const float); 	
	RGBColor operator*(const RGBColor&) const;
	bool operator==(const RGBColor&) const;				

	RGBColor PowC(float) const;
	float Average(void) const;

	RGBColor MaxToOne(RGBColor&) const;

	std::string ToString() {return "(" + std::to_string(r) + ", " + std::to_string(g) + ", " + std::to_string(b) + ")";}
};

// inlined non-member function

RGBColor operator*(const float a, const RGBColor& c);

inline RGBColor operator*(const float a, const RGBColor& c) 
{
	return (RGBColor (a * c.r, a * c.g, a * c.b));	
}


