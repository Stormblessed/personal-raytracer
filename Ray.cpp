#include "Ray.h"

Ray::Ray(void) : origin(0.0), direction(0.0, 0.0, 1.0) 
{
}

Ray::Ray(const Point3d& o, const Vector3d& d)
{
	origin = o;
	direction = d;
}

Ray::Ray(const Ray& r)
{
	origin = r.origin;
	direction = r.direction;
}

Ray::~Ray(void)
{
}

Ray& Ray::operator=(const Ray& rhs)
{
	if(this == &rhs) return *this;

	origin = rhs.origin;
	direction = rhs.direction;

	return *this;
}
