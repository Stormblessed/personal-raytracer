#pragma once
#include "Point3d.h"
#include "Vector3d.h"

class Ray
{
public:
	Point3d origin;
	Vector3d direction;

public:
	Ray(void);
	Ray(const Point3d&, const Vector3d&);
	Ray(const Ray&);
	~Ray(void);

	Ray& operator=(const Ray& rhs);
};

