#include "RayCast.h"
#include <iostream>

RayCast::RayCast(void)
{
}

RayCast::RayCast(World* _worldPtr) : Tracer(_worldPtr)
{
}

RayCast::~RayCast(void)
{
}

RGBColor RayCast::TraceRay(const Ray& ray) const 
{
	ShadeRec sr(world_ptr->HitObjects(ray));
		
	if (sr.hitAnObject) 
	{
		sr.ray = ray;
		return sr.material_ptr->Shade(sr);
	}   
	else return world_ptr->backgroundColor;
}

RGBColor RayCast::TraceRay(const Ray ray, const int depth) const 
{
	ShadeRec sr(world_ptr->HitObjects(ray));
		
	if(sr.hitAnObject) 
	{
		sr.ray = ray;
		return sr.material_ptr->Shade(sr);
	}   
	else return world_ptr->backgroundColor;
}
