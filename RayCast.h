#pragma once
#include "Tracer.h"
#include "World.h"

class RayCast : public Tracer
{
public:
	RayCast(void);
	RayCast(World*);
	virtual ~RayCast(void);

	virtual RGBColor TraceRay(const Ray& ray) const;
	virtual RGBColor TraceRay(const Ray ray, const int depth) const;
};


