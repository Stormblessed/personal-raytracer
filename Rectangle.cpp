#include "Rectangle.h"

const double Rectangle::kEpsilon = 0.001;

Rectangle::Rectangle(void) : GeometricObject(), corner(-1, 0, -1),  a(0, 0, 2), b(2, 0, 0), aLengthSquared(4.0),  bLengthSquared(4.0), normal(0, 1, 0), area(4.0), inverseArea(0.25), sampler_ptr(NULL)
{
}

Rectangle::Rectangle(const Point3d& _p0, const Vector3d& _a, const Vector3d& _b) : GeometricObject(), sampler_ptr(NULL)
{
	corner = _p0;
	a = _a;
	b = _b;
	aLengthSquared = a.LengthSquared();
	bLengthSquared = b.LengthSquared();
	area = a.Length() * b.Length();
	inverseArea = 1.0f / area;

	normal = a ^ b;
	normal.Normalize();
}

Rectangle::Rectangle(const Point3d& _p0, const Vector3d& _a, const Vector3d& _b, const Normal& n) : GeometricObject(), sampler_ptr(NULL)
{
	corner = _p0;
	a = _a;
	b = _b;
	aLengthSquared = a.LengthSquared();
	bLengthSquared = b.LengthSquared();
	area = a.Length() * b.Length();
	inverseArea = 1.0f / area;

	normal = n;
	normal.Normalize();
}

Rectangle::Rectangle(const Rectangle& r) : GeometricObject()
{
	corner = r.corner;
	a = r.a;
	b = r.b;
	aLengthSquared = r.aLengthSquared;
	bLengthSquared = r.bLengthSquared;
	area = r.area;
	inverseArea = r.inverseArea;

	normal = r.normal;
	normal.Normalize();

	if(r.sampler_ptr) sampler_ptr = r.sampler_ptr->Clone(); 
	else sampler_ptr = NULL;
}

Rectangle::~Rectangle(void) 
{
	if(sampler_ptr) 
	{
		delete sampler_ptr;
		sampler_ptr = NULL;
	}
}

Rectangle* Rectangle::Clone(void) const 
{
	return new Rectangle(*this);
}

Rectangle& Rectangle::operator=(const Rectangle& rhs)
{
	if(this == &rhs) return *this;

	GeometricObject::operator=(rhs);
	
	corner = rhs.corner;
	a = rhs.a;
	b = rhs.b;
	aLengthSquared = rhs.aLengthSquared;
	bLengthSquared = rhs.bLengthSquared;
	area = rhs.area;
	inverseArea = rhs.inverseArea;

	normal = rhs.normal;
	normal.Normalize();
	
	if(sampler_ptr) 
	{
		delete sampler_ptr;
		sampler_ptr = NULL;
	}

	if(rhs.sampler_ptr) sampler_ptr= rhs.sampler_ptr->Clone();

	return *this;
}

bool Rectangle::Hit(const Ray& ray, double& tmin, ShadeRec& sr) const 
{	
	double t = (corner - ray.origin) * normal / (ray.direction * normal); 
	
	if(t <= kEpsilon) return false;
			
	Point3d p = ray.origin + t * ray.direction;
	Vector3d d = p - corner;
	
	double ddota = d * a;
	
	if(ddota < 0.0 || ddota > aLengthSquared) return false;
		
	double ddotb = d * b;
	
	if(ddotb < 0.0 || ddotb > bLengthSquared) return false;
		
	tmin = t;
	sr.normal = normal;
	sr.localHitPoint = p;
	
	return true;
}


bool Rectangle::ShadowHit(const Ray& ray, float& tMin) const
{
	return false;
}

void Rectangle::SetSampler(Sampler* sampler) 
{
	sampler_ptr = sampler;
}

Point3d Rectangle::Sample(void) 
{
	Point2d samplePoint = sampler_ptr->SampleUnitSquare();
	return (corner + samplePoint.x * a + samplePoint.y * b);
}
					 
Normal Rectangle::GetNormal(const Point3d& p) 
{
	return normal;
}

float Rectangle::PDF(ShadeRec& sr) 
{	
	return inverseArea;
} 