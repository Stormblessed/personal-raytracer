#pragma once
#include "GeometricObject.h"
#include "Sampler.h"

class Rectangle : public GeometricObject
{
public:
	Rectangle(void);   											
	Rectangle(const Point3d& _p0, const Vector3d& _a, const Vector3d& _b);
	Rectangle(const Point3d& _p0, const Vector3d& _a, const Vector3d& _b, const Normal& n);
	Rectangle(const Rectangle& r); 	
	virtual	~Rectangle(void);   

	virtual Rectangle* Clone(void) const;
	Rectangle& operator= (const Rectangle& rhs);

	virtual bool Hit(const Ray& ray, double& t, ShadeRec& s) const;
	virtual bool ShadowHit(const Ray& ray, float& tMin) const;
				
	virtual void SetSampler(Sampler* sampler); 		
	virtual Point3d Sample(void);
	virtual Normal GetNormal(const Point3d& p);		
	virtual float PDF(ShadeRec& sr);
		
private:
	
	Point3d corner;
	Vector3d a;
	Vector3d b;
	double aLengthSquared;
	double bLengthSquared;
	Normal normal;	
		
	float area;
	float inverseArea;
	Sampler* sampler_ptr;	
		
	static const double kEpsilon;   											
};