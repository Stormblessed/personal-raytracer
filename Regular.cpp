#include "Regular.h"

Regular::Regular(void)
{
}

Regular::Regular(const int num) : Sampler(num) 
{
	GenerateSamples();
}

Regular::Regular(const Regular& r)	: Sampler(r) 
{
	GenerateSamples();
}

Regular::~Regular(void)
{
}

Regular& Regular::operator=(const Regular& rhs) 
{
	if(this == &rhs) return *this;
		
	Sampler::operator=(rhs);

	return *this;
}

Regular* Regular::Clone(void) const 
{
	return (new Regular(*this));
}

void Regular::GenerateSamples(void) 
{
	int n = (int) sqrt((float)numSamples);

	for(int j = 0; j < numSets; j++)
	{
		for(int p = 0; p < n; p++)		
		{
			for(int q = 0; q < n; q++)
			{
				samples.push_back(Point2d((q + 0.5) / n, (p + 0.5) / n));
			}
		}
	}
}

