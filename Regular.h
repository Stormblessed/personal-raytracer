#pragma once
#include "Sampler.h"

class Regular : public Sampler
{
public:
	Regular(void);
	Regular(const int);					
	Regular(const Regular&);	
	virtual ~Regular(void);

	Regular& operator=(const Regular& rhs);

	virtual Regular* Clone(void) const;	

private:
	virtual void GenerateSamples(void);
};

