#include "Sampler.h"
#include <algorithm> // for random_shuffle in Sampler::setup_shuffled_indices

Sampler::Sampler(void)
{
	numSamples = 1;
	numSets = 83;
	count = 0;
	jump = 0;
	samples.reserve(numSamples * numSets);
	SetupShuffledIndices();
}

Sampler::Sampler(const int ns)
{
	numSamples = ns;
	numSets = 83;
	count = 0;
	jump = 0;
	samples.reserve(numSamples * numSets);
	SetupShuffledIndices();
}

Sampler::Sampler(const int nSamples, const int nSets)
{
	numSamples = nSamples;
	numSets = nSets;
	count = 0;
	jump = 0;
	samples.reserve(numSamples * numSets);
	SetupShuffledIndices();
}

Sampler::Sampler(const Sampler& s)
{
	numSamples = s.numSamples;
	numSets = s.numSets;
	samples = s.samples;
	shuffledIndices = s.shuffledIndices;
	diskSamples = s.diskSamples;
	hemisphereSamples = s.hemisphereSamples;
	sphereSamples = s.sphereSamples;
	count = s.count;
	jump = s.jump;
	count = 0;
	jump = 0;
}

Sampler::~Sampler(void)
{
}

Sampler& Sampler::operator=(const Sampler& rhs)	
{
	if(this == &rhs) return *this;
	
	numSamples = rhs.numSamples;
	numSets = rhs.numSets;
	samples	= rhs.samples;
	shuffledIndices = rhs.shuffledIndices;
	diskSamples = rhs.diskSamples;
	hemisphereSamples = rhs.hemisphereSamples;
	sphereSamples = rhs.sphereSamples;
	count = rhs.count;
	jump = rhs.jump;
	
	return *this;
}

void Sampler::ShuffleXCoordinates(void) 
{
	for(int p = 0; p < numSets; p++)
	{
		for(int i = 0; i <  numSamples - 1; i++) 
		{
			int target = RandInt() % numSamples + p * numSamples;
			float temp = samples[i + p * numSamples + 1].x;
			samples[i + p * numSamples + 1].x = samples[target].x;
			samples[target].x = temp;
		}
	}
}

void Sampler::ShuffleYCoordinates(void) 
{
	for(int p = 0; p < numSets; p++)
	{
		for(int i = 0; i <  numSamples - 1; i++) 
		{
			int target = RandInt() % numSamples + p * numSamples;
			float temp = samples[i + p * numSamples + 1].y;
			samples[i + p * numSamples + 1].y = samples[target].y;
			samples[target].y = temp;
		}	
	}
}

void Sampler::SetupShuffledIndices(void) 
{
	shuffledIndices.reserve(numSamples * numSets);
	std::vector<int> indices;
	
	for(int j = 0; j < numSamples; j++)
	{
		indices.push_back(j);
	}
	
	for (int p = 0; p < numSets; p++) 
	{ 
		random_shuffle(indices.begin(), indices.end());	
		
		for (int j = 0; j < numSamples; j++)
		{
			shuffledIndices.push_back(indices[j]);
		}
	}	
}

void Sampler::MapSamplesToUnitDisk(void) 
{
	float r, phi;
	Point2d sp;
	
	diskSamples.reserve(samples.size());
		
	for(int j = 0; j < samples.size(); j++) 
	{
		 // map sample point to [-1, 1] X [-1,1]
		sp.x = 2.0 * samples[j].x - 1.0;	
		sp.y = 2.0 * samples[j].y - 1.0;
			
		if(sp.x > -sp.y) 
		{			// sectors 1 and 2
			if(sp.x > sp.y) 
			{		// sector 1
				r = sp.x;
				phi = sp.y / sp.x;
			}
			else 
			{					// sector 2
				r = sp.y;
				phi = 2 - sp.x / sp.y;
			}
		}
		else 
		{						// sectors 3 and 4
			if(sp.x < sp.y) 
			{		// sector 3
				r = -sp.x;
				phi = 4 + sp.y / sp.x;
			}
			else 
			{					// sector 4
				r = -sp.y;
				if(sp.y != 0.0)	// avoid division by zero at origin
					phi = 6 - sp.x / sp.y;
				else
					phi  = 0.0;
			}
		}
		
		phi *= PI / 4.0;
				
		diskSamples[j].x = r * cos(phi);
		diskSamples[j].y = r * sin(phi);
	}
	
	samples.erase(samples.begin(), samples.end());
}

void Sampler::MapSamplesToHemisphere(const float exp) 
{
	int size = samples.size();
	hemisphereSamples.reserve(numSamples * numSets);
		
	for(int j = 0; j < size; j++) 
	{
		float cos_phi = cos(2.0 * PI * samples[j].x);
		float sin_phi = sin(2.0 * PI * samples[j].x);	
		float cos_theta = pow((1.0 - samples[j].y), 1.0 / (exp + 1.0));
		float sin_theta = sqrt (1.0 - cos_theta * cos_theta);
		float pu = sin_theta * cos_phi;
		float pv = sin_theta * sin_phi;
		float pw = cos_theta;
		hemisphereSamples.push_back(Point3d(pu, pv, pw)); 
	}
}

void Sampler::MapSamplesToSphere(void) 
{
	float r1, r2;
	float x, y, z;
	float r, phi;
		
	sphereSamples.reserve(numSamples * numSets);   
		
	for(int j = 0; j < numSamples * numSets; j++) 
	{
		r1 	= samples[j].x;
    	r2 	= samples[j].y;
    	z 	= 1.0 - 2.0 * r1;
    	r 	= sqrt(1.0 - z * z);
    	phi = TWO_PI * r2;
    	x 	= r * cos(phi);
    	y 	= r * sin(phi);
		sphereSamples.push_back(Point3d(x, y, z)); 
	}
}

Point2d Sampler::SampleUnitSquare(void) 
{
	if(count % numSamples == 0) 
		jump = (RandInt() % numSets) * numSamples;	

	return (samples[jump + shuffledIndices[jump + count++ % numSamples]]);  
}

/*
// the first revised version in Listing in Listing 5.8
Point2D Sampler::sample_unit_square(void) 
{
	if (count % num_samples == 0)  									// start of a new pixel
		jump = (rand_int() % num_sets) * num_samples;				// random index jump initialised to zero in constructor
	
	return (samples[jump + count++ % num_samples]);	
}
*/

/*
// the original version in Listing 5.7
Point2D Sampler::sample_unit_square(void) 
{
	return (samples[count++ % (num_samples * num_sets)]);
}
*/

Point2d Sampler::SampleUnitDisk(void) 
{
	if (count % numSamples == 0)
		jump = (RandInt() % numSets) * numSamples;
	
	return (diskSamples[jump + shuffledIndices[jump + count++ % numSamples]]);
}

Point3d Sampler::SampleHemisphere(void) 
{
	if (count % numSamples == 0) 
		jump = (RandInt() % numSets) * numSamples;
		
	return (hemisphereSamples[jump + shuffledIndices[jump + count++ % numSamples]]);		
}

Point3d Sampler::SampleSphere(void) 
{
	if (count % numSamples == 0)
		jump = (RandInt() % numSets) * numSamples;
		
	return (sphereSamples[jump + shuffledIndices[jump + count++ % numSamples]]);		
}

Point2d Sampler::SampleOneSet(void) 
{
	return(samples[count++ % numSamples]);  
}

