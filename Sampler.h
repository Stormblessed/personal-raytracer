#pragma once
#include <vector>
#include "Point2d.h"
#include "Point3d.h"
#include "Constants.h"
#include "Utilities.h"

class Sampler
{
public:
	Sampler(void);
	Sampler(const int);							
	Sampler(const int, const int);		
	Sampler(const Sampler&);
	~Sampler(void);

	Sampler& operator=(const Sampler&);	
	virtual Sampler* Clone(void) const = 0;	

	void SetNumSets(const int ns) {numSets = ns;}
	int GetNumSamples(void) {return numSamples;}

	virtual void GenerateSamples(void) = 0;
	void SetupShuffledIndices(void);
	void ShuffleSamples(void);
	void ShuffleXCoordinates(void);
	void ShuffleYCoordinates(void);
	
	void MapSamplesToUnitDisk(void);
	void MapSamplesToHemisphere(const float);		
	void MapSamplesToSphere(void);	

	Point2d SampleUnitSquare(void);
	Point2d SampleUnitDisk(void);
	Point3d SampleHemisphere(void);
	Point3d SampleSphere(void);
	Point2d	SampleOneSet(void);

protected:
	int numSamples;
	int numSets;
	std::vector<Point2d> samples;
	std::vector<int> shuffledIndices;
	std::vector<Point2d> diskSamples;
	std::vector<Point3d> hemisphereSamples;	
	std::vector<Point3d> sphereSamples;	
	unsigned long count;
	int jump;
};																	

