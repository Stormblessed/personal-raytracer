#pragma once
#include "ViewPlane.h"
#include "RGBColor.h"
#include "Tracer.h"
#include "Camera.h"
#include <vector>
#include "Model.h"
#include "Light.h"

class Scene
{
public:
	Scene(void);
	~Scene(void);

	ViewPlane vp;
	RGBColor backgroundColor;

	Camera* camera_ptr;
	Light* ambient_ptr;

	std::vector<Model*> models;
	std::vector<Light*> lights;
};

