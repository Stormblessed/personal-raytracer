#include "SceneLoader.h"

SceneLoader::SceneLoader(void)
{
}

SceneLoader::~SceneLoader(void)
{
}

Scene* SceneLoader::LoadScene(std::string path, std::string fullMatPath)
{
	Scene* scene = new Scene();
	
	//Setup Sampler (DONE)
	int numSamples = 256;
	Jittered* sampler_ptr = new Jittered(numSamples);
	
	//Setup AmbientOccluder (DONE)
	AmbientOccluder* occluder_ptr = new AmbientOccluder();
	occluder_ptr->SetIntensity(1.0);
	occluder_ptr->SetColor(white);
	occluder_ptr->SetMinAmount(0.0);
	occluder_ptr->SetSampler(sampler_ptr);
	scene->ambient_ptr = occluder_ptr;

	//Parse through file
	//Grab Camera information (DONE)
	//Grab ViewPlane information (DONE)
	
	//Parse Lights (DONE)

	//Find 'o' object (DONE)
	//Walk from there and build a Model using similar logic to the below LoadOBJ logic (DONE)
	//Parse Verts (DONE)
	//Parse Normals (DONE)
	//Parse Faces (DONE)
	//Parse Material (DONE)
		//Set Model Material (DONE)
	//If 'o' found, start next object (DONE)

	//return Scene (DONE)

	Model* model = new Model();
	std::string currentModelName = "";
	int modelsEncounteredCount = 0;

	int vertexCountCorrection = 0;
	
	std::vector<Vector3d> vertexSet;
	std::vector<Vector3d> uvSet;
	std::vector<Vector3d> normalSet;
	std::vector<ModelFace> faceSet;

	std::string materialPath = "";

	FILE* file = fopen(path.c_str(), "r");
	if(file == NULL)
	{
		std::cout << path << std::endl;
		std::cout << path.c_str() << std::endl;
		perror("File could not be opened\n"); 
		return NULL;
	}

	while(true)
	{
		char lineHeader[128];
		int res = fscanf(file, "%s", lineHeader);
		if(res == EOF) break;

		if(strcmp(lineHeader, "cam") == 0)
		{
			float px = 0, py = 0, pz = 0;
			float lx = 0, ly = 0, lz = 0;
			float d = 0, z = 0;

			fscanf(file, "%f %f %f %f %f %f %f %f\n", &px, &py, &pz, &lx, &ly, &lz, &d, &z);
			
			Pinhole* pinhole = new Pinhole();
			pinhole->SetEye(px, py, pz);
			pinhole->SetLookAt(lx, ly, lz);
			pinhole->SetDistance(d);
			pinhole->SetZoom(z);
			pinhole->ComputeUVW();
			
			scene->camera_ptr = pinhole;
		}
		else if(strcmp(lineHeader, "vp") == 0)
		{
			float hr = 0, vr = 0;
			float ps = 0;
			float g = 0;

			fscanf(file, "%f %f %f %f\n", &hr, &vr, &ps, &g);

			scene->vp.SetHRes((int)hr);
			scene->vp.SetVRes((int)vr);
			scene->vp.SetPixelSize(ps);
			scene->vp.SetGamma(g);
			scene->vp.SetSamples(numSamples);
		}
		else if(strcmp(lineHeader, "light") == 0)
		{
			float px = 0, py = 0, pz = 0;
			float cr = 0, cg = 0, cb = 0;
			float i = 0;

			fscanf(file, "%f %f %f %f %f %f %f\n", &px, &py, &pz, &cr, &cg, &cb, &i);

			PointLight* light_ptr = new PointLight();
			light_ptr->SetColor(cr, cg, cb);
			light_ptr->SetIntensity(i);
			light_ptr->SetShadows(true);
			light_ptr->SetLocation(Vector3d(px, py, pz));
			
			scene->lights.push_back(light_ptr);
		}
		else if(strcmp(lineHeader, "mtllib") == 0)
		{
			char scannedChars[128];
			fscanf(file, "%s\n", scannedChars);
			materialPath = std::string(scannedChars);
		}
		else if(strcmp(lineHeader, "o") == 0)
		{
			char scannedChars[128];
			fscanf(file, "%s\n", scannedChars);

			std::string objName(scannedChars);

			if(currentModelName != "" && currentModelName != objName) //Model complete, finish off
			{
				model->Setup(faceSet, vertexSet, uvSet, normalSet);
				scene->models.push_back(model);

				vertexCountCorrection += vertexSet.size();

				faceSet.clear();
				vertexSet.clear();
				uvSet.clear();
				normalSet.clear();

				model = new Model();
			}
			else //start new model
			{
				model = new Model();
				currentModelName = objName;
			}
			modelsEncounteredCount++;
		}
		else if(strcmp(lineHeader, "usemtl") == 0)
		{
			char matName[128];
			fscanf(file, "%s\n", matName);
			model->material_ptr = LoadMaterial(materialPath, matName, fullMatPath);
		}
		else if(strcmp(lineHeader, "v") == 0)
		{
			Vector3d vertex;
			float x = 0, y = 0, z = 0;
			fscanf(file, "%f %f %f\n", &x, &y, &z);
			vertex.x = x;
			vertex.y = y;
			vertex.z = z;
			vertexSet.push_back(vertex);
		}
		else if(strcmp(lineHeader, "vt") == 0)
		{
			Vector3d uv;
			float x = 0, y = 0;
			fscanf(file, "%f %f\n", &x, &y);
			uv.x = x;
			uv.y = y;
			uvSet.push_back(uv);
		}
		else if(strcmp(lineHeader, "vn") == 0)
		{
			Vector3d normal;
			float x = 0, y = 0, z = 0;
			fscanf(file, "%f %f %f\n", &x, &y, &z);
			normal.x = x;
			normal.y = y;
			normal.z = z;
			normalSet.push_back(normal);
		}
		else if(strcmp(lineHeader, "f") == 0)
		{
			ModelFace face;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", 
									&vertexIndex[0], &uvIndex[0], &normalIndex[0],
									&vertexIndex[1], &uvIndex[1], &normalIndex[1],
									&vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			
			if(matches != 9) 
			{
				printf("File not formatted correctly\n"); 
				return NULL;
			} 

			face.vertexIndices.push_back(vertexIndex[0] - 1 - vertexCountCorrection);
			face.vertexIndices.push_back(vertexIndex[1] - 1 - vertexCountCorrection);
			face.vertexIndices.push_back(vertexIndex[2] - 1 - vertexCountCorrection);

			face.uvIndices.push_back(uvIndex[0] - 1 - vertexCountCorrection);
			face.uvIndices.push_back(uvIndex[1] - 1 - vertexCountCorrection);
			face.uvIndices.push_back(uvIndex[2] - 1 - vertexCountCorrection);

			face.normalIndices.push_back(normalIndex[0] - 1 - vertexCountCorrection);
			face.normalIndices.push_back(normalIndex[1] - 1 - vertexCountCorrection);
			face.normalIndices.push_back(normalIndex[2] - 1 - vertexCountCorrection);
			faceSet.push_back(face);
		}
	}

	if(scene->models.size() < modelsEncounteredCount) //Finish off last model
	{
		model->Setup(faceSet, vertexSet, uvSet, normalSet);
		scene->models.push_back(model);

		faceSet.clear();
		vertexSet.clear();
		uvSet.clear();
		normalSet.clear();
	}

	return scene;
}

Material* SceneLoader::LoadMaterial(std::string path, std::string materialName, std::string materialPath)
{
	Matte* matte_ptr = new Matte();
	matte_ptr->SetAmbientReflectCoeff(0.25f);
	matte_ptr->SetDiffuseReflectCoeff(0.20f);
	matte_ptr->SetDiffuseColor(RandFloatBetween(0.0f, 1.0f), RandFloatBetween(0.0f, 1.0f), RandFloatBetween(0.0f, 1.0f));

	std::string fullPath = "";
	if(materialPath != "")
	{
		fullPath = materialPath;
	}
	else
	{
		fullPath = "Input/" + path;
	}

	FILE* file = fopen((fullPath).c_str(), "r");
	if(file == NULL)
	{
		std::cout << fullPath << std::endl;
		perror("File could not be opened\n"); 
		return NULL;
	}

	while(true)
	{
		char lineHeader[128];
		int res = fscanf(file, "%s", lineHeader);
		if(res == EOF) break;

		if(strcmp(lineHeader, "newmtl") == 0)
		{
			char matName[128];
			fscanf(file, "%s\n", matName);
			if(strcmp(matName, materialName.c_str()) == 0)
			{
				while(true)
				{
					char attributeHeader[128];
					res = fscanf(file, "%s", attributeHeader);
					if(res == EOF) break;

					if(strcmp(attributeHeader, "Kd") == 0)
					{
						float r = 0, g = 0, b = 0;
						fscanf(file, "%f %f %f\n", &r, &g, &b);
						matte_ptr->SetDiffuseColor(r, g, b);
						return matte_ptr;
					}
				}
			}
		}
	}

	return matte_ptr;
}
