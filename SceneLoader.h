#pragma once
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "Model.h"
#include "ModelFace.h"
#include "Vector3d.h"
#include "Point3d.h"
#include "Material.h"
#include "Scene.h"
#include "Matte.h"

static class SceneLoader
{
public:
	SceneLoader(void);
	~SceneLoader(void);

	static Scene* LoadScene(std::string path, std::string fullMatPath);
	static Material* LoadMaterial(std::string path, std::string materialName, std::string fullPath);
};

