#include "ShadeRec.h"

ShadeRec::ShadeRec(World& w) : world(w), hitPoint(), localHitPoint(), normal(), ray(), direction(), material_ptr(NULL)
{
	hitAnObject = false;
	color = RGBColor(0.0f);
	depth = 0;
}

ShadeRec::ShadeRec(const ShadeRec& sr) : world(sr.world), material_ptr(sr.material_ptr)
{
	hitAnObject = sr.hitAnObject;
	hitPoint = sr.hitPoint;
	localHitPoint = sr.localHitPoint;
	normal = sr.normal;
	color = sr.color;
	ray = sr.ray;
	depth = sr.depth;
	direction = sr.direction;
}

ShadeRec::~ShadeRec(void)
{
}

ShadeRec& ShadeRec::operator=(const ShadeRec& rhs)
{
	if(this == &rhs)
		return (*this);

	hitAnObject = rhs.hitAnObject;
	localHitPoint = rhs.localHitPoint;
	normal = rhs.normal;
	color = rhs.color;
	material_ptr = rhs.material_ptr;

	return (*this);
}
