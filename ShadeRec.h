#pragma once
#include "Point3d.h"
#include "Normal.h"
#include "RGBColor.h"
#include "Ray.h"

class Material;
class World;

class ShadeRec
{
public:
	bool hitAnObject;
	Material* material_ptr;
	Point3d hitPoint;
	Point3d localHitPoint;
	Normal normal;
	RGBColor color;
	Ray ray;
	float t;
	int depth;
	Vector3d direction;
	World& world;

public:
	ShadeRec(World& w);
	ShadeRec(const ShadeRec&);
	~ShadeRec(void);

	ShadeRec& operator=(const ShadeRec& rhs);
};

