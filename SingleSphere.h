#pragma once
#include "Tracer.h"

class SingleSphere : public Tracer
{
public:
	SingleSphere(void);
	SingleSphere(World*);
	virtual ~SingleSphere(void);

	virtual RGBColor TraceRay(const Ray& ray) const;
};


