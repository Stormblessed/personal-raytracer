#include "Sphere.h"

const double Sphere::kEpsilon = 0.001;

Sphere::Sphere(void)
{
}

Sphere::Sphere(const Point3d p, double r)
{
	center = p;
	radius = r;
}

Sphere::~Sphere(void)
{
}

Sphere* Sphere::Clone(void) const 
{
	return new Sphere(this->center, this->radius);
}

void Sphere::SetCenter(Point3d p)
{
	center = p;
}

void Sphere::SetCenter(double x, double y, double z)
{
	center = Point3d(x, y, z);
}

void Sphere::SetRadius(double r)
{
	radius = r;
}

BBox Sphere::GetBoundingBox(void)
{
	Point3d bottomCorner = center - Vector3d(radius, radius, radius);
	Point3d topCorner = center + Vector3d(radius, radius, radius);
	return BBox(bottomCorner, topCorner);
}

bool Sphere::Hit(const Ray& ray, double& tMin, ShadeRec& sr) const
{
	double t;
	Vector3d diff = ray.origin - center;
	double a = ray.direction * ray.direction;
	double b = 2.0f * diff * ray.direction;
	double c = diff * diff - radius * radius;
	double disc = b * b - 4.0f * a * c;

	if(disc < 0.0f) return false;
	else
	{
		double e = sqrt(disc);
		double denom = 2.0 * a;
		t = (-b - e) / denom;

		if(t > kEpsilon)
		{
			tMin = t;
			sr.normal = (diff + t * ray.direction) / radius;
			sr.localHitPoint = ray.origin + t * ray.direction;
			return true;
		}

		t = (-b + e) / denom;

		if(t > kEpsilon)
		{
			tMin = t;
			sr.normal = (diff + t * ray.direction) / radius;
			sr.localHitPoint = ray.origin + t * ray.direction;
			return true;
		}
	}
	return false;
}

bool Sphere::ShadowHit(const Ray& ray, float& tMin) const
{
	double t;
	Vector3d diff = ray.origin - center;
	double a = ray.direction * ray.direction;
	double b = 2.0f * diff * ray.direction;
	double c = diff * diff - radius * radius;
	double disc = b * b - 4.0f * a * c;

	if(disc < 0.0f) return false;
	else
	{
		double e = sqrt(disc);
		double denom = 2.0 * a;
		t = (-b - e) / denom;

		if(t > kEpsilon)
		{
			tMin = t;
			return true;
		}

		t = (-b + e) / denom;

		if(t > kEpsilon)
		{
			tMin = t;
			return true;
		}
	}
	return false;
}

