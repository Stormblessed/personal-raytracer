#pragma once
#include "GeometricObject.h"
#include "Point3d.h"
#include "Normal.h"
#include <math.h>

class Sphere : public GeometricObject
{
public:
	Sphere(void);
	Sphere(const Point3d, const double);
	~Sphere(void);

	virtual Sphere* Clone(void) const;

	void SetCenter(Point3d);
	void SetCenter(double, double, double);
	void SetRadius(double);

	virtual bool Hit(const Ray& ray, double& tMin, ShadeRec& sr) const;
	virtual bool ShadowHit(const Ray& ray, float& tMin) const;

	virtual BBox GetBoundingBox(void);

private:
	Point3d center;
	double radius;
	static const double kEpsilon;
};

