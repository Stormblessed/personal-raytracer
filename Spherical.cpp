#include "Spherical.h"
#include "Constants.h"
#include <math.h>

Spherical::Spherical(void)
{
	lambdaMax = 1.0f;
	psiMax = 1.0f;
}

Spherical::Spherical(const Spherical& cam) : Camera(cam)	
{
	lambdaMax = cam.lambdaMax;
	psiMax = cam.psiMax;
}

Spherical::~Spherical(void)
{
}

Spherical& Spherical::operator=(const Spherical& rhs) 
{ 	
	if(this == &rhs) return *this;
		
	Camera::operator= (rhs);

	lambdaMax = rhs.lambdaMax;
	psiMax = rhs.psiMax;

	return *this;
}

Vector3d Spherical::RayDirection(const Point2d& pixelPoint, const int hRes, const int vRes, const float pixelSize) const
{
	Point2d normalPoint(2.0 / (pixelSize * hRes) * pixelPoint.x, 2.0 / (pixelSize * vRes) * pixelPoint.y);
	
	float lambda = normalPoint.x * lambdaMax * PI_ON_180;
	float psi = normalPoint.y * psiMax * PI_ON_180;

	float phi = PI -lambda;
	float theta = 0.5f * PI - psi;

	float sinPhi = sin(phi);
	float cosPhi = cos(phi);
	float sinTheta = sin(theta);
	float cosTheta = cos(theta);

	Vector3d dir = sinTheta * sinPhi * u + cosTheta * v + sinTheta * cosPhi * w;
	return dir;
}

void Spherical::Render(World& w)
{
	w.image.create(w.vp.hRes, w.vp.vRes, sf::Color::White);

	RGBColor radiance;
	ViewPlane vp(w.vp);
	Ray ray;
	int depth = 0;

	Point2d sampledPoint;
	Point2d pixelPoint;

	ray.origin = eye;

	for(int r = 0; r < vp.vRes; r++)
	{
		for(int c = 0; c < vp.hRes; c++)
		{
			radiance = black;

			for(int j = 0; j < vp.numSamples; j++)
			{
				sampledPoint = vp.sampler_ptr->SampleUnitSquare();
				pixelPoint.x = vp.pixelSize * (c - 0.5f * vp.hRes + sampledPoint.x);
				pixelPoint.y = vp.pixelSize * (r - 0.5f * vp.vRes + sampledPoint.y);
				
				ray.direction = RayDirection(pixelPoint, vp.hRes, vp.vRes, vp.pixelSize);

				radiance += w.tracer_ptr->TraceRay(ray, depth);
			}

			radiance /= vp.numSamples;
			radiance *= exposureTime;
			sf::Color resultColor = sf::Color(radiance.r * 255, radiance.g * 255, radiance.b * 255, 255);

			int screenX = c;
			int screenY = vp.vRes - r - 1;

			w.image.setPixel(screenX, screenY, resultColor);
		}
	}
}

void Spherical::SetLambdaMax(float lambda) 
{
	lambdaMax = lambda;
}	

void Spherical::SetPsiMax(float psi) 
{
	psiMax = psi;
}	