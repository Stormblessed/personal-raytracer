#pragma once
#include "Camera.h"
#include "World.h"

class Spherical : public Camera
{
public:
	Spherical(void);
	Spherical(const Spherical&);	
	virtual ~Spherical(void);

	Spherical& operator= (const Spherical&);	

	Vector3d RayDirection(const Point2d&, const int, const int, const float) const;
	virtual void Render(World&);

	void SetLambdaMax(float);
	void SetPsiMax(float);

private:
	float lambdaMax;
	float psiMax;
};

