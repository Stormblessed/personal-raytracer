#include "ThinLens.h"

ThinLens::ThinLens(void) : Camera()
{
	lensRadius = 1.0f;
	distance = 500.0f;
	focalDist = 250.0f;
	zoom = 1.0f;
	sampler_ptr = NULL;
}

ThinLens::ThinLens(const ThinLens& cam) : Camera(cam)	
{
	lensRadius = cam.lensRadius;
	distance = cam.distance;
	focalDist = cam.focalDist;
	zoom = cam.zoom;
	sampler_ptr = cam.sampler_ptr;
}

ThinLens::~ThinLens(void)
{
}

ThinLens& ThinLens::operator= (const ThinLens& rhs) 
{ 	
	if(this == &rhs) return *this;
		
	Camera::operator=(rhs);

	lensRadius = rhs.lensRadius;
	distance = rhs.distance;
	focalDist = rhs.focalDist;
	zoom = rhs.zoom;
	sampler_ptr = rhs.sampler_ptr;

	return *this;
}

Vector3d ThinLens::RayDirection(const Point2d& pixelPoint, const Point2d& lensPoint) const
{
	Point2d hitPoint;
	hitPoint.x = pixelPoint.x * focalDist / distance;
	hitPoint.y = pixelPoint.y * focalDist / distance;

	Vector3d dir = (hitPoint.x - lensPoint.x) * u + (hitPoint.y - lensPoint.y) * v - focalDist * w;
	dir.Normalize();
	return dir;
}

void ThinLens::Render(World& w)
{
	w.image.create(w.vp.hRes, w.vp.vRes, sf::Color::White);

	RGBColor radiance;
	ViewPlane vp(w.vp);
	Ray ray;
	int depth = 0;

	Point2d sampledPoint;
	Point2d pixelPoint;
	Point2d diskPoint;
	Point2d lensPoint;

	vp.pixelSize /= zoom;

	for(int r = 0; r < vp.vRes; r++)
	{
		for(int c = 0; c < vp.hRes; c++)
		{
			radiance = black;

			for(int j = 0; j < vp.numSamples; j++)
			{
				sampledPoint = vp.sampler_ptr->SampleUnitSquare();
				pixelPoint.x = vp.pixelSize * (c - 0.5f * vp.hRes + sampledPoint.x);
				pixelPoint.y = vp.pixelSize * (r - 0.5f * vp.vRes + sampledPoint.y);

				diskPoint = sampler_ptr->SampleUnitDisk();
				lensPoint = diskPoint * lensRadius;

				ray.origin = eye + lensPoint.x * u + lensPoint.y * v;
				ray.direction = RayDirection(pixelPoint, lensPoint);
				radiance += w.tracer_ptr->TraceRay(ray, depth);
			}

			radiance /= vp.numSamples;
			radiance *= exposureTime;
			sf::Color resultColor = sf::Color(radiance.r * 255, radiance.g * 255, radiance.b * 255, 255);

			int screenX = c;
			int screenY = vp.vRes - r - 1;

			w.image.setPixel(screenX, screenY, resultColor);
		}
	}
}

void ThinLens::SetSampler(Sampler* sampler)
{
	if(sampler_ptr)
	{
		delete sampler_ptr;
		sampler_ptr = NULL;
	}

	sampler_ptr = sampler;
	sampler_ptr->MapSamplesToUnitDisk();
}

void ThinLens::SetLensRadius(float radius)
{
	lensRadius = radius;
}

void ThinLens::SetDistance(float dist)
{
	distance = dist;
}

void ThinLens::SetFocalDist(float fDist)
{
	focalDist =fDist;
}

void ThinLens::SetZoom(float zoomFactor)
{
	zoom = zoomFactor;
}

