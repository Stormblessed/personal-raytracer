#pragma once
#include "Camera.h"
#include "Sampler.h"
#include "World.h"

class ThinLens : public Camera
{
public:
	ThinLens(void);
	ThinLens(const ThinLens&);	
	virtual ~ThinLens(void);

	ThinLens& operator= (const ThinLens&);

	Vector3d RayDirection(const Point2d&, const Point2d&) const;
	virtual void Render(World&);

	void SetSampler(Sampler*);
	void SetLensRadius(float);
	void SetDistance(float);
	void SetFocalDist(float);
	void SetZoom(float);

private:
	float lensRadius;
	float distance;
	float focalDist;
	float zoom;
	Sampler* sampler_ptr;
};

