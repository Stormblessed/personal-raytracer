#include "Tracer.h"
#include <iostream>

Tracer::Tracer(void)
{
	world_ptr = NULL;
}

Tracer::Tracer(World* _worldPtr)
{
	world_ptr = _worldPtr;
}

Tracer::~Tracer(void) 
{
	if(world_ptr) world_ptr = NULL;
}

RGBColor Tracer::TraceRay(const Ray& ray) const 
{
	return black;
}

RGBColor Tracer::TraceRay(const Ray ray, const int depth) const 
{
	return black;
}

RGBColor Tracer::TraceRay(const Ray ray, float& tMin, const int depth) const
{
	return black;
}

