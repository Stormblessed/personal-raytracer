#pragma once
#include "Constants.h"
#include "Ray.h"
#include "RGBColor.h"

class World;

class Tracer
{
public:
	Tracer(void);
	Tracer(World*);	
	virtual ~Tracer(void);

	virtual RGBColor TraceRay(const Ray& ray) const;
	virtual RGBColor TraceRay(const Ray ray, const int depth) const;
	virtual RGBColor TraceRay(const Ray ray, float& tMin, const int depth) const;

protected:
	World* world_ptr;
};
