#include "Triangle.h"

Triangle::Triangle (void) : GeometricObject(), v0(0, 0, 0), v1(0,0,1), v2(1,0,0), normal(0, 1, 0)
{
}

Triangle::Triangle(const Point3d& a, const Point3d& b, const Point3d& c) : GeometricObject(), v0(a), v1(b), v2(c)
{
	ComputeNormal();	
}

Triangle::Triangle (const Triangle& triangle) : GeometricObject(triangle), v0(triangle.v0), v1(triangle.v1), v2(triangle.v2), normal(triangle.normal)
{
}

Triangle::~Triangle(void) 
{
}

Triangle& Triangle::operator=(const Triangle& rhs) 
{
	if(this == &rhs) return *this;

	GeometricObject::operator=(rhs);

	v0 = rhs.v0;
	v1 = rhs.v1;
	v2 = rhs.v2;
	normal = rhs.normal;
		
	return *this;
}

Triangle* Triangle::Clone(void) const 
{
	return new Triangle(*this);
}

void Triangle::ComputeNormal(void) 
{
	normal = (v1 - v0) ^ (v2 - v0);  
	normal.Normalize();
}

BBox Triangle::GetBoundingBox(void) 
{
	double delta = 0.000001; 
	
	double x0 = Min(Min(v0.x, v1.x), v2.x) - delta;
	double x1 = Max(Max(v0.x, v1.x), v2.x) + delta;
	
	double y0 = Min(Min(v0.y, v1.y), v2.y) - delta;
	double y1 = Max(Max(v0.y, v1.y), v2.y) + delta;

	double z0 = Min(Min(v0.z, v1.z), v2.z) - delta;
	double z1 = Max(Max(v0.z, v1.z), v2.z) + delta;

	return BBox(x0, x1, y0, y1, z0, z1);
}

bool Triangle::Hit(const Ray& ray, double& tMin, ShadeRec& sr) const 
{	
	double a = v0.x - v1.x;
	double b = v0.x - v2.x;
	double c = ray.direction.x;
	double d = v0.x - ray.origin.x; 
	double e = v0.y - v1.y;
	double f = v0.y - v2.y;
	double g = ray.direction.y;
	double h = v0.y - ray.origin.y;
	double i = v0.z - v1.z;
	double j = v0.z - v2.z;
	double k = ray.direction.z;
	double l = v0.z - ray.origin.z;
		
	double m = f * k - g * j;
	double n = h * k - g * l;
	double p = f * l - h * j;
	double q = g * i - e * k;
	double s = e * j - f * i;
	
	double invDenom  = 1.0 / (a * m + b * q + c * s);
	
	double e1 = d * m - b * n - c * p;
	double beta = e1 * invDenom;
	
	if(beta < 0.0) return false;
	
	double r = r = e * l - h * i;
	double e2 = a * n + d * q + c * r;
	double gamma = e2 * invDenom;
	
	if(gamma < 0.0 ) return false;
	
	if(beta + gamma > 1.0) return false;
			
	double e3 = a * p - b * r + d * s;
	double t = e3 * invDenom;
	
	if(t < kEpsilon) return false;
					
	tMin = t;
	sr.normal = normal;  	
	sr.localHitPoint = ray.origin + t * ray.direction;	
	
	return true;	
}  		

bool Triangle::ShadowHit(const Ray& ray, float& tMin) const 
{	
	double a = v0.x - v1.x;
	double b = v0.x - v2.x;
	double c = ray.direction.x;
	double d = v0.x - ray.origin.x; 
	double e = v0.y - v1.y;
	double f = v0.y - v2.y;
	double g = ray.direction.y;
	double h = v0.y - ray.origin.y;
	double i = v0.z - v1.z;
	double j = v0.z - v2.z;
	double k = ray.direction.z;
	double l = v0.z - ray.origin.z;
		
	double m = f * k - g * j;
	double n = h * k - g * l;
	double p = f * l - h * j;
	double q = g * i - e * k;
	double s = e * j - f * i;
	
	double invDenom  = 1.0 / (a * m + b * q + c * s);
	
	double e1 = d * m - b * n - c * p;
	double beta = e1 * invDenom;
	
	if(beta < 0.0) return false;
	
	double r = r = e * l - h * i;
	double e2 = a * n + d * q + c * r;
	double gamma = e2 * invDenom;
	
	if(gamma < 0.0 ) return false;
	
	if(beta + gamma > 1.0) return false;
			
	double e3 = a * p - b * r + d * s;
	double t = e3 * invDenom;
	
	if(t < kEpsilon) return false;
					
	tMin = t;
	
	return true;	
}  