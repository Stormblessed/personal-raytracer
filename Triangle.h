#pragma once
#include "GeometricObject.h"
#include "Constants.h"
#include "MathHelper.h"

class Triangle: public GeometricObject {	
public:
	Triangle(void);   													
	Triangle(const Point3d& a, const Point3d& b, const Point3d& c);
	Triangle(const Triangle& triangle);
	~Triangle(void);   											

	virtual Triangle* Clone(void) const;
	Triangle& operator= (const Triangle& rhs);
		
	virtual BBox GetBoundingBox(void);
	void ComputeNormal(void);
		
	virtual bool Hit(const Ray& ray, double& tMin, ShadeRec& sr) const;		 					 
	virtual bool ShadowHit(const Ray& ray, float& tMin) const; 
		
private:
	
	Point3d	v0, v1, v2;
	Normal	normal;
};
