#include "Utilities.h"

int RandInt()
{
	return (rand());
}

int RandInt(int l, int h) 
{
	return ((int) (RandFloat(0, h - l + 1) + l));
}

float RandFloat()
{
	return ((float)rand() / (float)RAND_MAX);
}

float RandFloat(int l, float h) 
{
	return (RandFloat() * (h - l) + l);
}

void SeedRand(const int seed)
{
	srand(seed);
}

float RandFloatBetween(float min, float max)
{
	return min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max - min)));
}