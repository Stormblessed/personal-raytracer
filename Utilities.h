#pragma once
#include <stdlib.h>
#include <time.h> 

int RandInt();
int	RandInt(int, int);  
float RandFloat();
float RandFloat(int, float);	
void SeedRand(const int);
float RandFloatBetween(float, float);

