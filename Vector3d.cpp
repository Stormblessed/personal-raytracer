#include <math.h>
#include "Vector3D.h"
#include "Normal.h"
#include "Point3D.h"
#include <iostream>

Vector3d::Vector3d(void)
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
}

Vector3d::Vector3d(double a)						
{
	x = a;
	y = a;
	z = a;
}

Vector3d::Vector3d(double _x, double _y, double _z)
{
	x = _x;
	y = _y;
	z = _z;
}

Vector3d::Vector3d(const Vector3d& v)
{
	x = v.x;
	y = v.y;
	z = v.z;
}

Vector3d::Vector3d(const Normal& n)
{
	x = n.x;
	y = n.y;
	z = n.z;
}

Vector3d::Vector3d(const Point3d& p)
{
	x = p.x;
	y = p.y;
	z = p.z;
}

Vector3d::~Vector3d(void) 							
{
}

Vector3d& Vector3d::operator=(const Vector3d& rhs) 
{
	if(this == &rhs) return *this;

	x = rhs.x; 
	y = rhs.y; 
	z = rhs.z;

	return *this;
}

Vector3d& Vector3d::operator=(const Normal& rhs) 
{
	x = rhs.x; 
	y = rhs.y; 
	z = rhs.z;
	return *this;
}

Vector3d& Vector3d::operator=(const Point3d& rhs) 
{
	x = rhs.x; 
	y = rhs.y; 
	z = rhs.z;
	return *this;
}

Vector3d Vector3d::operator-(void) const 
{
	return (Vector3d(-x, -y, -z));    
}

Vector3d Vector3d::operator*(const double a) const 
{	
	return (Vector3d(x * a, y * a, z * a));	
}

Vector3d Vector3d::operator/(const double a) const 
{	
	return (Vector3d(x / a, y / a, z / a));	
}

Vector3d Vector3d::operator+(const Vector3d& v) const 
{
	return (Vector3d(x + v.x, y + v.y, z + v.z));
}

Vector3d Vector3d::operator-(const Vector3d& v) const 
{
	return (Vector3d(x - v.x, y - v.y, z - v.z));
}

double Vector3d::operator*(const Vector3d& v) const 
{
	return (x * v.x + y * v.y + z * v.z);
} 

Vector3d Vector3d::operator^(const Vector3d& v) const 
{
	return (Vector3d(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x));
}

Vector3d& Vector3d::operator+=(const Vector3d& v) 
{
	x += v.x; y += v.y; z += v.z;
	return (*this);
}

double Vector3d::LengthSquared(void) 
{	
	return (x * x + y * y + z * z);
}

double Vector3d::Length(void) 
{
	return (sqrt(x * x + y * y + z * z));
}

void Vector3d::Normalize(void) 
{	
	double length = sqrt(x * x + y * y + z * z);
	x /= length;
	y /= length;
	z /= length;
}

Vector3d& Vector3d::Hat(void) 
{	
	double length = sqrt(x * x + y * y + z * z);
	x /= length; 
	y /= length; 
	z /= length;
	return *this;
}

// non-member function

Vector3d operator*(const Matrix& mat, const Vector3d& v) 
{
	return (Point3d(mat.m[0][0] * v.x + mat.m[0][1] * v.y + mat.m[0][2] * v.z,
					mat.m[1][0] * v.x + mat.m[1][1] * v.y + mat.m[1][2] * v.z,
					mat.m[2][0] * v.x + mat.m[2][1] * v.y + mat.m[2][2] * v.z));
}
