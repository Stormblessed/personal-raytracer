#pragma once
#include "Matrix.h"

class Normal;
class Point3d;

class Vector3d
{
public:
	double	x, y, z;

public:
	Vector3d(void);											
	Vector3d(double a);	
	Vector3d(double _x, double _y, double _z);	
	Vector3d(const Vector3d& v);
	Vector3d(const Normal& n);	
	Vector3d(const Point3d& p);	
	~Vector3d (void);

	Vector3d& operator= (const Vector3d& rhs);
	Vector3d& operator= (const Normal& rhs); 
	Vector3d& operator= (const Point3d& rhs); 
	Vector3d operator- (void) const;									
	Vector3d operator* (const double a) const;		
	Vector3d operator/ (const double a) const;
	Vector3d operator+ (const Vector3d& v) const;
	Vector3d& operator+= (const Vector3d& v);					
	Vector3d operator- (const Vector3d& v) const;					
	double operator* (const Vector3d& b) const;
	Vector3d operator^ (const Vector3d& v) const;

	double Length(void);
	double LengthSquared(void);
	void Normalize(void); 
	Vector3d& Hat(void);
};

// inlined non-member function

Vector3d operator*(const double a, const Vector3d& v);

inline Vector3d operator*(const double a, const Vector3d& v) 
{
	return (Vector3d(a * v.x, a * v.y, a * v.z));	
}

// non-inlined non-member function

Vector3d operator* (const Matrix& mat, const Vector3d& v);
