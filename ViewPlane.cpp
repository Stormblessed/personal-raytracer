#include "ViewPlane.h"

ViewPlane::ViewPlane(void) : sampler_ptr(NULL)
{
	hRes = 400;
	vRes = 400;
	pixelSize = 1.0f;
	gamma = 1.0f;
	invGamma = 1.0f;
	numSamples = 1.0f;
	maxDepth = 10;
}

ViewPlane::ViewPlane(const ViewPlane& vp)
{
	hRes = vp.hRes;
	vRes = vp.vRes;
	pixelSize = vp.pixelSize;
	gamma = vp.gamma;
	invGamma = vp.invGamma;
	numSamples = vp.numSamples;
	sampler_ptr = vp.sampler_ptr;
	maxDepth = vp.maxDepth;
}

ViewPlane::~ViewPlane(void)
{
}

ViewPlane& ViewPlane::operator= (const ViewPlane& rhs) 
{
	if(this == &rhs) return *this;
		
	hRes = rhs.hRes;
	vRes = rhs.vRes;
	pixelSize = rhs.pixelSize;
	gamma = rhs.gamma;
	invGamma = rhs.invGamma;
	numSamples = rhs.numSamples;
	sampler_ptr = rhs.sampler_ptr;
	maxDepth = rhs.maxDepth;
	
	return *this;
}

void ViewPlane::SetHRes(const int hres) 
{
	hRes = hres;
}

void ViewPlane::SetVRes(const int vres) 
{
	vRes = vres;
}

void ViewPlane::SetPixelSize(const float size) 
{
	pixelSize = size;
}

void ViewPlane::SetGamma(const float g) 
{
	gamma = g;
	invGamma = 1.0 / gamma;
}

void ViewPlane::SetMaxDepth(const int d)
{
	maxDepth = d;
}

void ViewPlane::SetNumSamples(const int n)
{
	numSamples = n;
}

void ViewPlane::SetSampler(Sampler* s)
{
	if(sampler_ptr)
	{
		delete sampler_ptr;
		sampler_ptr = NULL;
	}

	numSamples = s->GetNumSamples();
	sampler_ptr = s;
}

void ViewPlane::SetSamples(const int n)
{
	numSamples = n;
	
	if(sampler_ptr)
	{
		delete sampler_ptr;
		sampler_ptr = NULL;
	}

	if(numSamples > 1) sampler_ptr = new MultiJittered(numSamples);
	else sampler_ptr = new Regular(1);
}
