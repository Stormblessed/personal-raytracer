#pragma once
#include "Sampler.h"
#include "MultiJittered.h"
#include "Regular.h"

class ViewPlane
{
public:
	int hRes;
	int vRes;
	float pixelSize;
	float gamma;
	float invGamma;
	int maxDepth;

	int numSamples;
	Sampler* sampler_ptr;

public:
	ViewPlane(void);
	ViewPlane(const ViewPlane&);
	~ViewPlane(void);

	ViewPlane& operator=(const ViewPlane&);

	void SetHRes(const int);
	void SetVRes(const int);	
	void SetPixelSize(const float);
	void SetGamma(const float);
	void SetNumSamples(const int);
	void SetMaxDepth(const int);

	void SetSampler(Sampler*);
	void SetSamples(const int);
};
