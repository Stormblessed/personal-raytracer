#include "World.h"
#include <iostream>
#include <string>

//Build Functions

//#include "BuildRedSphere.cpp"

//World Member Function Definitions

World::World(void) : tracer_ptr(NULL), camera_ptr(NULL), ambient_ptr(new Ambient())
{
	backgroundColor = black;
}

World::~World(void)
{
	if(tracer_ptr) 
	{
		delete tracer_ptr;
		tracer_ptr = NULL;
	}
	if(camera_ptr)
	{
		delete camera_ptr;
		camera_ptr = NULL;
	}
}

/*
//Simple sphere build
void World::Build(void)
{
	vp.SetHRes(200);
	vp.SetVRes(200);
	vp.SetPixelSize(1.0f);
	vp.SetGamma(1.0f);
	backgroundColor = black;
	tracer_ptr = new SingleSphere(this);
	sphere.SetCenter(0.0f);
	sphere.SetRadius(85.0f);
}*/

/* ThinLens DepthOfField setup build example 
	
	ThinLens* thinLens_ptr = new ThinLens();
	thinLens_ptr->SetSampler(new MultiJittered(numSamples));
	thinLens_ptr->SetEye(0, 6, 50);
	thinLens_ptr->SetLookAt(0, 6, 0);
	thinLens_ptr->SetDistance(40.0f);
	thinLens_ptr->SetFocalDist(74.0f);
	thinLens_ptr->SetLensRadius(1.0f);
	thinLens_ptr->ComputeUVW();
	camera_ptr = thinLens_ptr;
*/

/*
//Pinhole camera test build
void World::Build(void)
{
	Pinhole* pinhole_ptr = new Pinhole();
	pinhole_ptr->SetEye(0, 0, 150);
	pinhole_ptr->SetLookAt(0, 0, -50);
	pinhole_ptr->SetDistance(50);
	pinhole_ptr->ComputeUVW();
	//pinhole_ptr->SetZoom(2.0f);
	camera_ptr = pinhole_ptr;

	vp.SetHRes(800);
	vp.SetVRes(800);
	vp.SetPixelSize(1.0f);
	vp.SetGamma(1.0f);

	vp.SetSampler(new Jittered(25));

	backgroundColor = black;
	tracer_ptr = new MultipleObjects(this);
	
	Sphere* sphere_ptr = new Sphere();
	sphere_ptr->SetCenter(0, -25, 0);
	sphere_ptr->SetRadius(80);
	sphere_ptr->SetColor(1, 0, 0);
	AddObject(sphere_ptr);

	sphere_ptr = new Sphere(Point3d(0, 30, 0), 60);
	sphere_ptr->SetColor(1, 1, 0);
	AddObject(sphere_ptr);

	Plane* plane_ptr = new Plane(Point3d(0, 0, 0), Normal(0, 1, 1));
	plane_ptr->SetColor(0.0, 0.3, 0.0);
	AddObject(plane_ptr);
}
*/

/*
//Materials example build
void World::Build(void)
{
	Pinhole* pinhole_ptr = new Pinhole();
	pinhole_ptr->SetEye(0, 0, 500);
	pinhole_ptr->SetLookAt(-5, 0, 0);
	pinhole_ptr->SetDistance(850.0f);
	pinhole_ptr->SetZoom(2.0f);
	pinhole_ptr->ComputeUVW();
	camera_ptr = pinhole_ptr;

	vp.SetHRes(600);
	vp.SetVRes(600);
	vp.SetPixelSize(1.0f);
	vp.SetGamma(1.0f);
	vp.SetSamples(16);

	backgroundColor = black;
	tracer_ptr = new RayCast(this);

	Ambient* ambient = new Ambient();
	ambient->SetIntensity(1.0f);
	ambient_ptr = ambient;

	PointLight* pointLight = new PointLight();
	pointLight->SetLocation(Vector3d(100, 50, 150));
	pointLight->SetColor(1, 0.6, 0);
	pointLight->SetIntensity(3.0f);
	AddLight(pointLight);

	
	Matte* matte = new Matte();
	matte->SetAmbientReflectCoeff(0.25f);
	matte->SetDiffuseReflectCoeff(0.65f);
	matte->SetDiffuseColor(0.5f, 0.5f, 0.5f);
	

	Phong* phong = new Phong();
	phong->SetAmbientReflectCoeff(0.25f);
	phong->SetDiffuseReflectCoeff(0.65f);
	phong->SetSpecularReflectCoeff(0.20f);
	phong->SetDiffuseColor(0.5f, 0.5f, 0.5f);
	phong->SetSpecularExponent(10);
	
	Sphere* sphere_ptr = new Sphere(Point3d(10, -5, 0), 27);
	sphere_ptr->SetMaterial(matte);
	AddObject(sphere_ptr);
}
*/

/*Shadows example build
void World::Build(void)
{
	Pinhole* pinhole_ptr = new Pinhole();
	pinhole_ptr->SetEye(0, 0, 500);
	pinhole_ptr->SetLookAt(-5, 0, 0);
	pinhole_ptr->SetDistance(850.0f);
	pinhole_ptr->SetZoom(2.0f);
	pinhole_ptr->ComputeUVW();
	camera_ptr = pinhole_ptr;

	vp.SetHRes(600);
	vp.SetVRes(600);
	vp.SetPixelSize(1.0f);
	vp.SetGamma(1.0f);
	vp.SetSamples(16);

	backgroundColor = black;
	tracer_ptr = new RayCast(this);

	Ambient* ambient = new Ambient();
	ambient->SetIntensity(1.0f);
	ambient_ptr = ambient;

	PointLight* pointLight = new PointLight();
	pointLight->SetLocation(Vector3d(100, 50, 150));
	pointLight->SetColor(1, 1, 1);
	pointLight->SetIntensity(3.0f);
	pointLight->SetShadows(true);
	AddLight(pointLight);

	Matte* orangeMatte = new Matte();
	orangeMatte->SetAmbientReflectCoeff(0.25f);
	orangeMatte->SetDiffuseReflectCoeff(0.65f);
	orangeMatte->SetDiffuseColor(1.0f, 0.6f, 0.0f);

	Matte* blueMatte = new Matte();
	blueMatte->SetAmbientReflectCoeff(0.25f);
	blueMatte->SetDiffuseReflectCoeff(0.65f);
	blueMatte->SetDiffuseColor(0.3f, 0.3f, 0.9f);

	Phong* phong = new Phong();
	phong->SetAmbientReflectCoeff(0.25f);
	phong->SetDiffuseReflectCoeff(0.65f);
	phong->SetSpecularReflectCoeff(0.20f);
	phong->SetDiffuseColor(0.5f, 0.5f, 0.5f);
	phong->SetSpecularExponent(10);
	
	Grid* grid_ptr = new Grid();

	Sphere* sphere_ptr = new Sphere(Point3d(50, 50, 0), 27);
	sphere_ptr->SetMaterial(orangeMatte);
	grid_ptr->AddObject(sphere_ptr);

	sphere_ptr = new Sphere(Point3d(25, 25, -80), 35);
	sphere_ptr->SetMaterial(blueMatte);
	grid_ptr->AddObject(sphere_ptr);

	sphere_ptr = new Sphere(Point3d(0, 0, -180), 43);
	sphere_ptr->SetMaterial(orangeMatte);
	grid_ptr->AddObject(sphere_ptr);

	sphere_ptr = new Sphere(Point3d(-50, -50, -400), 100);
	sphere_ptr->SetMaterial(blueMatte);
	grid_ptr->AddObject(sphere_ptr);

	grid_ptr->SetupCells();
	AddObject(grid_ptr);
}
*/

/*ambient occlusion build
void World::Build(void)
{
	Pinhole* pinhole = new Pinhole();
	pinhole->SetEye(25, 20, 45);
	pinhole->SetLookAt(0, 1, 0);
	pinhole->SetDistance(5000);
	pinhole->SetZoom(0.66f);
	pinhole->ComputeUVW();
	camera_ptr = pinhole;

	int numSamples = 256;

	vp.SetHRes(300);
	vp.SetVRes(300);
	vp.SetPixelSize(1.0f);
	vp.SetGamma(1.0f);
	vp.SetSamples(numSamples);

	tracer_ptr = new RayCast(this);

	Jittered* sampler_ptr = new Jittered(256);

	AmbientOccluder* occluder_ptr = new AmbientOccluder();
	occluder_ptr->SetIntensity(1.0);
	occluder_ptr->SetColor(white);
	occluder_ptr->SetMinAmount(0.0);
	occluder_ptr->SetSampler(sampler_ptr);
	ambient_ptr = occluder_ptr;

	Matte* matte_1 = new Matte();
	matte_1->SetAmbientReflectCoeff(0.75f);
	matte_1->SetDiffuseReflectCoeff(0);
	matte_1->SetDiffuseColor(1.0f, 1.0f, 0.0f);

	//Box* box = new Box(Point3d(0.0, 0.0f, 0.0f), Point3d(1.0, 1.0f, 1.0f));
	//box->SetMaterial(matte_1);
	//AddObject(box);

	Grid* grid_ptr = new Grid();

	Sphere* sphere = new Sphere(Point3d(0, 1, 0), 1);
	sphere->SetMaterial(matte_1);
	AddObject(sphere);
	//grid_ptr->AddObject(sphere);

	Matte* matte_2 = new Matte();
	matte_2->SetAmbientReflectCoeff(0.75f);
	matte_2->SetDiffuseReflectCoeff(0);
	matte_2->SetDiffuseColor(lightGray);

	Plane* plane = new Plane(Point3d(0.0f), Normal(0.0f, 1.0f, 0.0f));
	plane->SetMaterial(matte_2);
	AddObject(plane);
	
	//grid_ptr->SetupCells();
	//AddObject(grid_ptr);
}
*/

/* Grid Build
void World::Build(void)
{
	Pinhole* pinhole = new Pinhole();
	pinhole->SetEye(25, 20, 45);
	pinhole->SetLookAt(0, 1, 0);
	pinhole->SetDistance(5000);
	pinhole->SetZoom(1.5f);
	pinhole->ComputeUVW();
	camera_ptr = pinhole;

	int numSamples = 256;

	vp.SetHRes(600);
	vp.SetVRes(600);
	vp.SetPixelSize(1.0f);
	vp.SetGamma(1.0f);
	vp.SetSamples(numSamples);

	tracer_ptr = new RayCast(this);

	Jittered* sampler_ptr = new Jittered(numSamples);

	AmbientOccluder* occluder_ptr = new AmbientOccluder();
	occluder_ptr->SetIntensity(1.0);
	occluder_ptr->SetColor(white);
	occluder_ptr->SetMinAmount(0.0);
	occluder_ptr->SetSampler(sampler_ptr);
	ambient_ptr = occluder_ptr;

	PointLight* light_ptr = new PointLight();
	light_ptr->SetColor(1.0f, 1.0f, 1.0f);
	light_ptr->SetIntensity(9.0f);
	light_ptr->SetShadows(true);
	light_ptr->SetLocation(Vector3d(0.0f, 3.0f, 0.0f));
	AddLight(light_ptr);

	int numSpheres = 5;
	float volume = 0.1f / numSpheres;
	float radius = pow(0.75f * volume / PI, 0.3333333f);

	Grid* grid_ptr = new Grid();
	SeedRand(15);

	for(int i = 0; i < numSpheres; i++)
	{
		Matte* matte_ptr = new Matte();
		matte_ptr->SetAmbientReflectCoeff(0.75f);
		matte_ptr->SetDiffuseReflectCoeff(0);
		matte_ptr->SetDiffuseColor(RandFloatBetween(0.0f, 1.0f), RandFloatBetween(0.0f, 1.0f), RandFloatBetween(0.0f, 1.0f));

		Sphere* sphere_ptr = new Sphere();
		sphere_ptr->SetRadius(radius);
		sphere_ptr->SetCenter(RandFloatBetween(-1.0f, 1.0f), RandFloatBetween(0.0f + radius, 2.0f), RandFloatBetween(-1.0f, 1.0f));
		sphere_ptr->SetMaterial(matte_ptr);
		grid_ptr->AddObject(sphere_ptr);
	}

	Matte* matte_2 = new Matte();
	matte_2->SetAmbientReflectCoeff(0.75f);
	matte_2->SetDiffuseReflectCoeff(0);
	matte_2->SetDiffuseColor(lightGray);

	Rectangle* rect = new Rectangle(Point3d(-1.0f, 0.0f, -1.0f), Vector3d(1.0f, 0.0f, 0.0f), Vector3d(0.0f, 0.0f, 1.0f));
	rect->SetMaterial(matte_2);
	grid_ptr->AddObject(rect);

	grid_ptr->SetupCells();
	this->AddObject(grid_ptr);
}
*/

void World::BuildFromScene(std::string path, std::string materialPath)
{
	SeedRand(15);

	scene_ptr = SceneLoader::LoadScene(path, materialPath);

	camera_ptr = scene_ptr->camera_ptr;
	vp = scene_ptr->vp;
	tracer_ptr = new RayCast(this);
	ambient_ptr = scene_ptr->ambient_ptr;

	for(int i = 0; i < scene_ptr->lights.size(); i++)
	{
		AddLight(scene_ptr->lights[i]);
	}

	Grid* grid_ptr = new Grid();

	for(int i = 0; i < scene_ptr->models.size(); i++)
	{
		for(int f = 0; f < scene_ptr->models[i]->faces.size(); f++)
		{
			ModelFace face = scene_ptr->models[i]->faces[f];
			int vertices[3];
			for(int v = 0; v < face.vertexIndices.size(); v++)
			{
				vertices[v] = face.vertexIndices[v];
			}
			ModelTriangle* triangle = new ModelTriangle();
			triangle->model_ptr = scene_ptr->models[i];
			triangle->index0 = vertices[0];
			triangle->index1 = vertices[1];
			triangle->index2 = vertices[2];
			triangle->SetMaterial(scene_ptr->models[i]->material_ptr);
			triangle->ComputeNormal(false);

			grid_ptr->AddObject(triangle);
		}
	}

	grid_ptr->SetupCells();

	this->AddObject(grid_ptr);
}

void World::Render(void) const
{
	sf::Image result;
	RenderOnto(result);
	result.saveToFile("test2.png");
}

void World::RenderOnto(sf::Image& result) const
{
	result.create(vp.hRes, vp.vRes, sf::Color::White);

	RGBColor pixelColor;
	Ray ray;
	double zw = 100.0f;

	Point2d sampledPoint;
	Point2d pixelPoint;

	ray.direction = Vector3d(0.0f, 0.0f, -1.0f);

	for(int r = 0; r < vp.vRes; r++)
	{
		for(int c = 0; c < vp.hRes; c++)
		{
			pixelColor = black;

			for(int j = 0; j < vp.numSamples; j++)
			{
				sampledPoint = vp.sampler_ptr->SampleUnitSquare();
				pixelPoint.x = vp.pixelSize * (c - 0.5f * vp.hRes + sampledPoint.x);
				pixelPoint.y = vp.pixelSize * (r - 0.5f * vp.vRes + sampledPoint.y);

				ray.origin = Point3d(pixelPoint.x, pixelPoint.y, zw);
				pixelColor += tracer_ptr->TraceRay(ray);
			}

			pixelColor /= vp.numSamples;
			sf::Color resultColor = sf::Color(pixelColor.r * 255, pixelColor.g * 255, pixelColor.b * 255, 255);

			int screenX = c;
			int screenY = vp.vRes - r - 1;
			result.setPixel(screenX, screenY, resultColor);
		}
	}
}

void World::RenderOntoPerspective(sf::Image& result) const
{
	result.create(vp.hRes, vp.vRes, sf::Color::White);

	RGBColor pixelColor;
	Ray ray;

	Point2d sampledPoint;
	Point2d pixelPoint;

	ray.origin = Point3d(0.0f, 0.0f, 150.0f);

	for(int r = 0; r < vp.vRes; r++)
	{
		for(int c = 0; c < vp.hRes; c++)
		{
			pixelColor = black;

			for(int j = 0; j < vp.numSamples; j++)
			{
				sampledPoint = vp.sampler_ptr->SampleUnitSquare();
				pixelPoint.x = vp.pixelSize * (c - 0.5f * vp.hRes + sampledPoint.x);
				pixelPoint.y = vp.pixelSize * (r - 0.5f * vp.vRes + sampledPoint.y);
				ray.direction = Vector3d(pixelPoint.x, pixelPoint.y, -50);
				ray.direction.Normalize();

				pixelColor += tracer_ptr->TraceRay(ray);
			}

			pixelColor /= vp.numSamples;
			sf::Color resultColor = sf::Color(pixelColor.r * 255, pixelColor.g * 255, pixelColor.b * 255, 255);

			int screenX = c;
			int screenY = vp.vRes - r - 1;
			result.setPixel(screenX, screenY, resultColor);
		}
	}
}

void World::OpenWindow(void)
{
}

void World::DisplayPixel(const int row, const int column, const RGBColor& pixelColor) const
{
}

ShadeRec World::HitBareBonesObjects(const Ray& ray)
{
	ShadeRec sr(*this);
	double t;
	double tMin = kHugeValue;

	for(int i = 0; i < objects.size(); i++)
	{
		if(objects[i]->Hit(ray, t, sr) && (t < tMin))
		{
			sr.hitAnObject = true;
			tMin = t;
			sr.color = objects[i]->GetColor();
		}
	}

	return sr;
}

ShadeRec World::HitObjects(const Ray& ray)
{
	ShadeRec sr(*this);
	double t;
	Normal normal;
	Point3d localHitPoint;
	double tMin = kHugeValue;

	for(int i = 0; i < objects.size(); i++)
	{
		if(objects[i]->Hit(ray, t, sr) && (t < tMin))
		{
			sr.hitAnObject = true;
			tMin = t;
			sr.material_ptr = objects[i]->GetMaterial();
			sr.hitPoint = ray.origin + t * ray.direction;
			normal = sr.normal;
			localHitPoint = sr.localHitPoint;
			sr.color = objects[i]->GetColor();
		}
	}

	if(sr.hitAnObject)
	{
		sr.t = tMin;
		sr.normal = normal;
		sr.localHitPoint = localHitPoint;
	}

	return sr;
}