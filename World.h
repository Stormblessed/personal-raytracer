#pragma once
#include <SFML\Graphics.hpp>
#include <vector>

#include "Constants.h"
#include "Utilities.h"
#include "RGBColor.h"
#include "Sphere.h"
#include "Box.h"
#include "Plane.h"
#include "Rectangle.h"
#include "Tracer.h"
#include "ViewPlane.h"
#include "Vector3d.h"
#include "Point2d.h"
#include "Point3d.h"
#include "Normal.h"
#include "GeometricObject.h"
#include "Ray.h"
#include "SingleSphere.h"
#include "MultipleObjects.h"
#include "RayCast.h"
#include "Sampler.h"
#include "Jittered.h"
#include "Camera.h"
#include "Pinhole.h"
#include "ThinLens.h"
#include "Orthographic.h"
#include "Light.h"
#include "Ambient.h"
#include "AmbientOccluder.h"
#include "PointLight.h"
#include "Material.h"
#include "Matte.h"
#include "Phong.h"
#include "Emissive.h"
#include "Grid.h"
#include "SceneLoader.h"
#include "ModelTriangle.h"

class World
{
public:
	Scene* scene_ptr;

	ViewPlane vp;
	RGBColor backgroundColor;

	Tracer* tracer_ptr;
	Camera* camera_ptr;
	Light* ambient_ptr;

	std::vector<GeometricObject*> objects;
	std::vector<Light*> lights;

	sf::Image image;

public:
	World(void);
	~World(void);

	void Build(void);
	void BuildFromScene(std::string path, std::string materialPath);
	void Render(void) const;
	void RenderOnto(sf::Image&) const;
	void RenderOntoPerspective(sf::Image&) const;
	void OpenWindow(void);
	void DisplayPixel(const int row, const int column, const RGBColor& pixelColor) const;

	void AddObject(GeometricObject* obj_ptr) {objects.push_back(obj_ptr);}
	void AddLight(Light* light_ptr) {lights.push_back(light_ptr);}

	ShadeRec HitBareBonesObjects(const Ray& ray);
	ShadeRec HitObjects(const Ray& ray);
};

