#include <SFML/Graphics.hpp>
#include <iostream>
#include "World.h"

int GlobalWidth = 800;
int GlobalHeight = 800;

void RenderSceneAtPath(std::string path, std::string matPath, std::string outputPath);

int main(int argc, char* argv[])
{
	if(argc == 2)
	{
		std::string path(argv[1]);
		RenderSceneAtPath(path, "", "");
	}
	else if(argc == 4)
	{
		std::string path(argv[1]);
		std::string matPath(argv[2]);
		std::string outputPath(argv[3]);
		RenderSceneAtPath(path, matPath, outputPath);
	}
	else
	{
		std::cout << "Error, invalid console input." << std::endl;
		system("PAUSE");
	}

    return 0;
}

void RenderSceneAtPath(std::string path, std::string matPath, std::string outputPath)
{
	World world;
	world.BuildFromScene(path, matPath);
	world.camera_ptr->Render(world);
	world.image.saveToFile(outputPath + "output.png");
}

